<?php

class view_auth extends view {
	public function generateAuthPage() {
		include __DIR__."/../templates/timetable/tteditor/login.html";
	}

	public function generateAuthPageWithMessage( $class, $message ) {
		echo "<div class='".$class."'>".$message."</div>";
		include __DIR__."/../templates/timetable/tteditor/login.html";
	}
}

?>