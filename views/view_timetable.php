<?php
class timetableParser {
	private static $subjectsTime = array( 
		"8:30" => 0,
		"8:15" => 0,
		"10:15" => 1,
		"12:00" => 2,
		"12:15" => 2,
		"13:50" => 3,
	);

	public static $arrTimetable = array(
		0 => array(),
		1 => array(),
		2 => array(),
		3 => array(),
		4 => array(),
		5 => array()
	);
	
	public function __construct($timetable) {
		var_dump($timetable);
		foreach( $timetable['subjects'] as $subject ) {
			self::$arrTimetable[ $subject['subject_weekday'] ][self::$subjectsTime[$subject['subject_time_start']]] = $subject;
		}
	}

	public function getTimetable() {
		return self::$arrTimetable;
	}

}

class view_timetable extends view {

	public function generateTimetableForm($form) {
		include __DIR__."/../templates/timetable/".$form.".php";
	}

	public function generateError($errorText) {
		echo "<div class='error'>".$errorText."</div>";
	}
	
	public function generateOk() {
		echo "<div class='success'>Расписание добавлено</div>";
	}

	public function generateTimetablePage( $template, $data, $scriptsAndStyles = array("scriptList" => array(), "styleList" => array()) ) {
		$this->generateHeader($scriptsAndStyles);
		include __DIR__."/../templates/timetable/".$template.".php";
		$this->generateFooter();
	}

	public function showTimetable($timetable) {
		$timetable = new timetableParser($timetable);
		$timetable = $timetable->getTimetable();
		$this->generateTimetablePage( "groupTimetable", $timetable, array("styleList" => array("timetableStyle")) );
	}

	public function showMessage($message, $type) {
		$this->generateHeader();
		echo "<div class='".$type."'>".$message."</div>";
		$this->generateFooter();
	}
	
}

?>
