<?php

class view_profile extends view {
	public function generateModule($moduleName, $data = null) {
		include __DIR__."/../templates/profileModules/".$moduleName.".php";
	}

	public function generateProfilePage($modules) {
		$this->generateHeader();
		foreach( $modules as $moduleName => $data ) {
			$this->generateModule($moduleName, $data);
		}
		$this->generateFooter();
	}

	public function generateUnautorizedUserPage() {
		$this->generateHeader();
		echo "Вы не авторизованы!<br>";
		echo "<a href='/auth'>Перейти на страницу авторизации</a>";
		$this->generateFooter();
	}
}

?>