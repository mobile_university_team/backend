<?php
session_start();

include_once "core/controller.php";
include_once "core/model.php";
include_once "core/view.php";

include_once "core/error.php";
include_once "core/functions.php";
include_once "core/mongodb.php";
include_once "core/user.php";
//include_once "core/db.php";

include_once "route.php";
Route::start();
?>
