<?php

class view{
	public function __construct(){}

	public function generate($template, $data = null){
		include __DIR__."/../templates/".$template.".php";
	}

	public function generateHeader( $scriptsAndStyles = array( "scriptList" => array(), "styleList" => array() ) ) {
		include __DIR__."/../templates/header.php";
	}

	public function generateFooter() {
		include __DIR__."/../templates/footer.php";
	}

	public function generateFullPage( $template, $data = null, $scriptsAndStyles = array( "scriptList" => array(), "styleList" => array() ) ) {
		$this->generateHeader($scriptsAndStyles);
		$this->generate($template, $data);
		$this->generateFooter();
	}

	public function generatePageWithMessage( $template, $type, $messageText, $data = null, $scriptsAndStyles = array( "scriptList" => array(), "styleList" => array() ) ) {
		$this->generateHeader($scriptsAndStyles);
		echo "<div class='".$type."'>".$messageText."</div>";
		$this->generate($template, $data);
		$this->generateFooter();
	}
}

function generateView($template, $data = null) {
	$view = new view();
	$view->generate($template, $data);
}

?>
