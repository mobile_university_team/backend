<?php

class mDB{
	
	static private $instance;
	static private $dataBase;

	private function __construct(){

		if(is_null(self::$dataBase)){
			$conf = include __DIR__."/../config/db_config.php";
			$host = $conf['host'];
			$dbname = $conf['db']['mongo'];
			self::$dataBase = new MongoClient($host);
			self::$dataBase = self::$dataBase->selectDB($dbname);
		}
	}

	private function __sleep() {}
	private function __wakeup() {}


	public static function getInstance() {
		if( is_null( self::$instance ) )
			self::$instance = new self();
		return self::$instance;
	}

	public function insert( $collectionName, $array, $unique = false ) {
		if( $unique && 
			$this->isInCollection($collectionName, $unique) ) {
			return false;
		}
		$collection = self::$dataBase->selectCollection($collectionName);
		$array['id'] = $this->incrementId($collectionName);
		$collection->insert($array);
		return $array['id'];
	}

	public function remove( $collectionName, $array ) {
		$collection = self::$dataBase->selectCollection($collectionName);
		$collection->remove($array);
	}

	public function update( $collectionName, $whatUpdate, $howUpdate) {
		$collection = self::$dataBase->selectCollection( $collectionName );
		$collection->update($whatUpdate, $howUpdate);
	}
	
	public function isInCollection( $collectionName, $array ) {
		$collection = self::$dataBase->selectCollection($collectionName);
		$cur = $collection->find( $array );
		return $cur->hasNext();
	}

	public function getCount( $collectionName, $query ) {
		$collection = self::$dataBase->selectCollection($collectionName);
		$cur = $collection->find( $query );
		$num = $cur->count();
		return $num;
	}

	public function find( $collectionName, $query, $limit = null, $sortBy = null, $offset = null) {
		$collection = self::$dataBase->selectCollection($collectionName);
		$cur = $collection->find( $query );
		$res = array();
		

		if( !is_null($limit) ) {
			$cur->limit($limit);
		}
		
		if( !is_null($offset) ) {
			$cur->skip($offset);
		}
		
		if( !is_null($sortBy) ) {
			$cur->sort(array("$sortBy" => -1));
		}

		foreach($cur as $ar) {
			$res[] = $ar;
		}
		return $res;
	}

	public function isExists($collectionName, $query) {
		$mas = $this->find($collectionName, $query);
		return !empty($mas);
	}

	public function incrementId( $collectionName ) {
		$collection = self::$dataBase->selectCollection( "ids" );
		if ( !$this->isExists("ids", array("collection" => $collectionName)) ) {
			$collection->insert( array("collection" => $collectionName, "lastId" => 0) );
		}
		$id = $collection->findAndModify( array("collection" => $collectionName), array('$inc' => array("lastId" => 1)), null, array("new" => true) );
		return $id['lastId'];
	}
}

?>