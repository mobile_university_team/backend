<?php

class controller{
	protected $model;
	protected $view;

	public function __construct() {
		$view = "view_";
		$view = $view.request::getController();
		if( class_exists($view) ) {
			$this->view = new $view;
		} else {
			$this->view = new view();
		}

		$model = "model_";
		$model = $model.request::getController();
		if( class_exists($model) ) {
			$this->model = new $model;
		} else {
			$this->model = new model();
		}
	}
}

?>