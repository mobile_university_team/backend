<?php

function filterSqlRequest($request) {
	/*
	strip_tags(str);
	htmlspecialchars(string);
	*/
}

function addScript($name) {
	echo "<script src='/scripts/".$name.".js'></script>";
}

function addStyle($name) {
	echo '<link rel="stylesheet" type="text/css" href="/styles/'.$name.'.css">';
}

function isMac() {
	if( preg_match('/apple|iphone|ipod|ipad|mac|macbook|macos/i', $_SERVER['HTTP_USER_AGENT']) ) {
		return true;
	}
	return false;
}

function isEmpty($ob) {
	if( is_array($ob) ) {
		foreach( $ob as $key => $val) {
			if( is_array($val) ) {
				foreach( $val as $k => $v ) {
					if( !isEmpty($v) ) {
						return false;
					}
				}
				return true;
			}
			if( is_object($val) ) {
				$val = (array) $val;
				foreach( $val as $k => $v ) {
					if( !isEmpty($v) ) {
						return false;
					}
				}
				return true;
			}
			if( !empty($val) ) {
				return false;
			}
		}
		return true;
	}

	if( is_object($ob) ) {
		$ob = (array) $ob;
		foreach( $ob as $key => $val ) {
			if( !isEmpty($val) ) {
				return false;
			}
		}
		return true;
	}

	return empty($ob);
}

function putLog($file, $message) {
	$f = fopen($file, "a");
	fputs($f, $message."\n");
}

function hasEmpty( $data ) {
	foreach( $data as $key => $val ) {
		if( empty($val) ) {
			return true;
		}
	}
	return false;
}

function apiRequest($action, $type = "GET", $data = null) {
	$ch = curl_init("http://".$_SERVER['SERVER_NAME']."/api/".API_VERSION."/".$action);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	if( $type == "POST" ) {
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	}

	$ans = curl_exec($ch);
	$ans_arr = json_decode($ans, true);
	if( is_null($ans_arr) ) {
		return $ans;
	} else {
		return $ans_arr;
	}
}

function apiAnswer($array) {
	echo json_encode($array);
}

function isLogged() {
	$uid = "";
	$token = "";
	if( isset($_COOKIE['uid']) &&
		isset($_COOKIE['token']) ) {

		$uid = $_COOKIE['uid'];
		$token = $_COOKIE['token'];
	} else {
		return false;
	}
	
	$hash = hash("sha512", $uid.$token);
	$logged =  apiRequest("isLogged", "POST", array( 
		"uid" => $uid,
		"hash" => $hash)
	);
	$logged = $logged['logged'];
	return $logged;
}

function isAdmin() {
	if( isset($_COOKIE['uid']) &&
		isset($_COOKIE['token']) ) {

		$uid = $_COOKIE['uid'];
		$token = $_COOKIE['token'];
		$hash = hash("sha512", $uid.$token);

		$rights = apiRequest("getRights", "POST", array( 
			"uid" => $uid,
			"hash" => $hash)
		);

		if( $rights['logged'] && $rights['admin']) {
			return true;
		} else {
			return false;
		}

	} else {
		return false;
	}
}

function isHeadman() {
	if( isset($_COOKIE['uid']) &&
		isset($_COOKIE['token']) ) {

		$uid = $_COOKIE['uid'];
		$token = $_COOKIE['token'];
		$hash = hash("sha512", $uid.$token);

		$rights = apiRequest("getRights", "POST", array( 
			"uid" => $uid,
			"hash" => $hash)
		);
		return $rights['headman'];
	} else {
		return false;
	}
}

?>
