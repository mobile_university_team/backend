<?php

class User{
	
	private function __construct() {}
	private function __sleep() {}
	private function __wakeup() {}

	public static function getRights($uid) {
		$user = User::getUser($uid);
		if( isset($user['rights']) ) {
			return $user['rights'];
		}
		return false;
	}

	public static function getUser($uid) {
		$t = mDB::getInstance()->find( "users", array("id" => (int) $uid) );
		if( isset($t[0]) ) {
			return $t[0];
		}
		return false;
	}
	
	public static function getToken($uid) {
		if( User::isValidToken($uid) ) {
			$user = User::getUser($uid);
			if( $user && !empty($user['token']) ) {
				return $user["token"];
			}
		}
		return false;
	}

	public static function isValidToken($uid) {
		$user = User::getUser($uid);
		if ( !$user ) {
			return false;
		}
		if( !isset($user['token']) ) {
			return false;
		}
		$token = $user['token'];
		if( !empty($token) ) {
			if( $token['time'] > time() ) {
					return true;
			} else {
				mDB::getInstance()->update("users", array("id" => $uid), array('unset' => array("token" => "")));
				return false;
			}
		}
		return false;
	}

	public static function isAdmin($uid) {
		$user = User::getUser($uid);
		if( $user ) {
			return $user['rights'] === "admin";
		}
		return false;
	}

	
	public static function setUser($email, $token, $time) {
		$userQuery = array("email" => $email);
		$tokenQuery = array('$set' => array('token' => array("token" => $token,  "time" => $time)));
		mDB::getInstance()->update("users", $userQuery, $tokenQuery);
		$uid = mDB::getInstance()->find("users", $userQuery);
		$uid = $uid[0]['id'];
		return $uid;
	}

	// public static function logOut($uid) {
	// 	$user = User::getUser($uid);
		
	// 	if( !$user ) {
	// 		return false;
	// 	}
	// 	foreach( $user['tokens'] as $key => $val ) {
	// 		if( isset($val[$tokenKey]['token']) ) {
	// 			unset($user['tokens'][$key]);
	// 		}
	// 	}

	// 	mDB::getInstance()->update("users", array("id" => (int) $uid), array('$set' => array("tokens" => $user['tokens'])));
	// 	return true;		// apiAnswer(array($user['tokens'])); die();
		// mDB::getInstance()->insert( "users", $user );
	// }


	public static function isHeadman($uid) {
		$user = User::getUser($uid);
		if( isset($user['headman']) ) {
			return $user['headman'] === true;
		}
		return false;
	}

	public static function getGroup($uid) {
		$user = User::getUser($uid);
		if( isset($user['group']) ) {
			return $user['group'];
		}
		return false;
	}
}


?>
