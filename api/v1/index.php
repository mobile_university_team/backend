<?php
define("FAIL", "failed");
define("OK", "ok");

function accessIdentify($data){
	if( !isset($data['uid']) || !isset($data['hash']) ) {
		return false;
	}
	$uid = $data['uid'];
	$hash = $data['hash'];

	unset($data['uid']);
	unset($data['hash']);


	foreach($data as $key=>$val) {
		$data[$key] = strtolower($val);
	}

	sort($data, SORT_STRING);
	

	$token = User::getToken($uid);
	if( $token === false ) {
		return false;
	}
	$token = $token['token'];
	
	$identifyStr = $uid;
	$identifyStr .= $token;
	foreach($data as $key=>$val) {
		$identifyStr .= $val;
	}

	$ansHash = hash("sha512", $identifyStr);
	return $hash === $ansHash;
}

function getDataFromEncryptedString($str) {
	$data = "";
	$str = base64_decode($str);
	$res = openssl_private_decrypt($str, $data, file_get_contents(__DIR__."/mobile_university_rsa_1024_private.pem"));
	
	if ( $res ) {
		return $data;
	} else {
		return false;
	}
}

class Subject {
	
	public function __construct($name, $type, $weekday, $parity, $number, $break, $group_name, $auditory) {
		$this->subject_name = $name;
		$this->subject_type = $type;
		$this->subject_weekday = $weekday;
		$this->subject_parity = $parity;
		$this->subject_number = $number;
		$this->subject_break = $break;
		$this->subject_group_name = $group_name;
		$this->auditory_name = $auditory;
	}

	public $subject_name;
	public $subject_type;
	public $subject_weekday;
	public $subject_parity;
	public $subject_number;
	public $subject_break;
	public $subject_group_name;
	public $auditory_name;
}

class Teacher {

	public function __construct($name, $date_start, $date_end) {
		$this->name = $name;
		$this->date_start = $date_start;
		$this->date_end = $date_end;
		$this->subjects = array();
	}

	public function addNewSubject($subject) {
		if( !($subject instanceof Subject) ) {
			return;
		}
		if( !$this->isInList($subject) ) {
			$this->subjects[] = $subject;
		}
	}

	public function isInList($subject) {
		foreach( $this->subjects as $key => $sub ) {
			if( $subject->subject_name === $sub->subject_name &&
				$subject->subject_number === $sub->subject_number &&
				$subject->subject_weekday === $sub->subject_weekday
				/* $subject->subject_type === $sub->subject_type */ ) {
				return true;
			}
		}
		return false;
	}

	public $name;
	public $subjects;
	public $date_start;
	public $date_end;

}

class Starter {

	public static function start() {
		
		$action = request::getQuery();
		$action = $action[0];

		$api = new api();
		if( method_exists($api, $action) ) {
			$api->$action();
		} else {
			apiAnswer( array("status" => "no such method") );
			return;
		}
	}

}

class api {
	
	public function getPublicKey() {
		apiAnswer(array("key" => file_get_contents(__DIR__."/mobile_university_pub.pem")));
	}

	public function auth() {

		if( !isset($_POST['data']) || empty($_POST['data']) ) {
			apiAnswer(array("status" => "no data"));
			return;
		}

		$data = getDataFromEncryptedString( $_POST['data'] );

		if( !$data ) {
			apiAnswer(array("status" => "data corrupted"));
			return;
		}

		$_POST = json_decode($data, true);

		$email = "";
		$pass = "";

		if( isset($_POST['email']) && !empty($_POST['email']) ) {
			$email = $_POST['email'];
		} else {
			apiAnswer( array("status" => FAIL, "message" => "Укажите email") );
			return;
		}

		if( isset($_POST['pass']) && !empty($_POST['pass']) ) {
			$pass = hash("sha512", $_POST['pass']);
		} else {
			apiAnswer( array("status" => FAIL, "message" => "Укажите пароль") );
			return;
		}

		$query = array("email" => $email, "pass" => $pass);

		try {
			$user = mDB::getInstance()->find("users", $query);
		} catch( Exception $e) {
			putLog( LOG_PATH, $e->getMessage() );
			die();
		}

		if( !empty($user) ) {
			$user = $user[0];
			if( !empty($user['token']) ) {
				apiAnswer( array("status" => OK, "token" => $user['token']['token'], "uid" => $user['id']) );
			} else {
				$time = time();
				$timeTo = $time+(3600*3600*3600);
				$token = hash("sha512", $email.$pass.$time);
				$uid = User::setUser( $email, $token, $timeTo );
				apiAnswer( array("status" => OK, "token" => $token, "uid" => $uid) );
			}
		} else {
			apiAnswer( array("status" => FAIL, "message" => "Такого пользователя не существует") );
		}
	}

	public function registerNewUser() {

		// if( !isset($_POST['data']) || empty($_POST['data']) ) {
		// 	apiAnswer(array("status" => "no data"));
		// 	return;
		// }

		// $data = getDataFromEncryptedString( $_POST['data'] );

		// if( !$data ) {
		// 	apiAnswer(array("status" => "data corrupted"));
		// 	return;
		// }
		
		// $_POST = json_decode($data, true);
		
		if( isset($_POST['headman']) && $_POST['headman'] !== false && $_POST['headman'] !== "false" ) {
			$headmanIsAlreadyExists = mDB::getInstance()->isInCollection( "users", array("group" => $_POST['group'], "faculty" => $_POST['faculty'], "department" => $_POST['department'], "semester" => $_POST['semester'], "headman" => true) );
			if( $headmanIsAlreadyExists ) {
				apiAnswer(array("status" => FAIL, "message" => "Регистрация прервана! В вашей группе уже есть староста! Обратитесь к нему, чтобы он передал вам полномочия") );
				return;
			} else {
				$_POST['headman'] = true;
			}
		}
		
//		if( empty($_POST['postfix']) ) {
//			unset( $_POST['postfix'] );
//		}

		if( !isset($_POST['email']) || empty($_POST['email']) ) {
			apiAnswer( array("status" => FAIL, "message" => "Не указан email") );
			return;
		}
		
		if( !preg_match('/.+@.+\..+/', $_POST['email']) ) {
			apiAnswer(array("status" => FAIL, "message" => "мыло не валидно!"));
			return;
		}

		if( !isset($_POST['pass']) || empty($_POST['pass']) ) {
			apiAnswer( array("status" => FAIL, "message" => "Укажите пароль") );
			return;
		}
		
		foreach( $_POST as $key => $val) {
			if( empty($val) ) {
				apiAnswer(array("status" => FAIL, "message" => "Одно из полей пропущено") );
				return;
			}
		}
		

		if( mDB::getInstance()->isInCollection("users", array("email" => $_POST['email'])) ) {
			apiAnswer(array("status" => FAIL, "message" => "Такой пользователь уже существует") );
			return;
		}
		
		$_POST['pass'] = hash("sha512", $_POST['pass']);
		$_POST['rights'] = "reader";

		try {
			$res = mDB::getInstance()->insert("users", $_POST);
		} catch( Exception $e ) {
			putLog( LOG_PATH, $e->getMessage() );
			die();
		}
		mail($_POST['email'], "Спасибо за регистрацию", "Спасибо что решили воспользоваться нашим приложением! Надеемся вам понравится!", 'From: e-university@tp-dev.com'."\r\n" . 'Content-type: text/html; charset=utf8'."\r\n");
		apiAnswer( array("status" => OK) );
	}

	public function isLogged() {
		if( accessIdentify($_POST) ) {
			$uid = $_POST['uid'];
			$hash = $_POST['hash'];
			$valid = User::isValidToken($uid);
			if( $valid ) {
				apiAnswer(array("logged" => true));
			} else {
				apiAnswer(array("logged" => false));
			}
		} else {
			apiAnswer(array("logged" => false));
		}
	}

	public function getRights() {
		if( accessIdentify($_POST) ) {
			$rights = array(
				"admin" => User::isAdmin($_POST['uid']),
				"headman" => User::isHeadman($_POST['uid'])
			);
			apiAnswer($rights);
		} else {
			apiAnswer( array( 
				"admin" => false,
				"headman" => false) 
			);
		}
	}
	public function getProfile() {
		if( accessIdentify($_POST) ) {
			$profile = User::getUser($_POST['uid']);
			//$profile = json_encode($profile);
			apiAnswer(array("status" => OK, "profile" => $profile));
		} else {
			apiAnswer(array("status" => "access denied"));
		}
	}
	public function getTimetable() {
		if( accessIdentify($_POST) ) {
			
			$user = User::getUser($_POST['uid']);
			if( !$user ) {
				apiAnswer(array("status" => FAIL));
				return;
			}

			$faculty = $user['faculty'];
			$department = $user['department'];
			$semester = $user['semester'];
			$group = $user['group'];
			//$postfix = $user['postfix'];

			$timetable = mDB::getInstance()->find("groupsTimetable", array( "group" => $group, 'faculty' => $faculty, "department" => $department, "semester" => $semester ));
			
			if( empty($timetable) ) {
				apiAnswer(array("status" => "no timetable"));
				return;
			}
			
			if( !isset($timetable[0]['timetable']) || is_null($timetable[0]['timetable'])) {
				apiAnswer(array("status" => "no timetable"));
				return;
			}
			
			apiAnswer(array("status" => OK, "timetable" => $timetable[0]['timetable']));
		} else {
			apiAnswer(array("status" => "access denied"));	
		}
	}

	public function setTimetable() {
		// if( accessIdentify($_POST) ) {
			if( isset($_POST['timetable']) ) {
				$_POST['timetable'] = json_decode($_POST['timetable']);
				if( isEmpty($_POST['timetable']) ) {
					apiAnswer(array("status" => "null timetable"));
					return;
				}
			} else {
				apiAnswer(array("status" => "null timetable"));
				return;
			}
			$uid = $_POST['uid'];
			if( User::isHeadman($uid) ) {
				$user = User::getUser($_POST['uid']);
				if( !$user ) {
					apiAnswer(array("status" => FAIL));
					return;
				}

				$faculty = $user['faculty'];
				$department = $user['department'];
				$semester = $user['semester'];
				$group = $user['group'];
				//$postfix = $user['postfix'];

				if( !mDB::getInstance()->isInCollection("groupsTimetable", array( "group" => $group, 'faculty' => $faculty, "department" => $department, "semester" => $semester )) ) {
					mDB::getInstance()->insert( "groupsTimetable", array("timetable" => $_POST['timetable'], "group" => $group, 'faculty' => $faculty, "department" => $department, "semester" => $semester) );
					apiAnswer( array("status" => OK) );
					return;
				}
				// 	mDB::getInstance()->remove("groupsTimetable", array("group" => $group));
				mDB::getInstance()->update( "groupsTimetable", array( "group" => $group, 'faculty' => $faculty, "department" => $department, "semester" => $semester ), array('$set' => array("timetable" => $_POST['timetable'])) );
				apiAnswer( array("status" => OK) );
			} else {
				apiAnswer( array("status" => "not headman") );
			}
		// } else {
			// apiAnswer(array("status" => "access denied"));
		// }
	}

	public function updateSubject() {
		if( accessIdentify($_POST) ) {
			
		}
	}

	public function getOrganizationsDatabaseVersion() {
		apiAnswer(array("status" => OK, "version" => 2));
	}

	public function getOrganizationsDatabase( $file = "orgDb.db") {
	    if (ob_get_level()) {
	      ob_end_clean();
	    }
	    header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename='.basename($file));
	    header('Content-Transfer-Encoding: binary');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($file));
	    readfile($file);
	    exit;
	}

	public function getTeacherList() {
		$timetables = mDB::getInstance()->find( "groupsTimetable", array());
		$teacherList = array();

		foreach( $timetables as $key => $t ) {
			$t = $t['timetable']['subjects'];
			if( empty($t) ) {
				continue;
			}
			foreach( $t as $subKey => $subject ) {
				foreach( $subject['subject_groups'] as $gNum => $group ) {
					if( !in_array($group['teacher_name'], $teacherList)) {
						$teacherList[] = $group['teacher_name'];
					}
				}
			}
		}
		apiAnswer( array("status" => OK, "teachers" => $teacherList));
	}
	
	public function getTeacherTimetableByName() {
		
		$name;
		$teacher;
		
		if( isset($_GET['name']) && !empty($_GET['name']) ) {
			$name = $_GET['name'];
		} else {
			apiAnswer( array("status" => FAIL, "message" => "Не указано имя преподавателя") );
			return;
		}

		$timetables = mDB::getInstance()->find( "groupsTimetable", array("timetable.subjects.subject_groups.teacher_name" => $name) );
		if( empty($timetables) ) {
			apiAnswer(array("status" => FAIL, "message" => "no teacher"));
			return;
		}

		foreach( $timetables as $key => $t ) {
			$s = $t['timetable']['subjects'];
			if( empty($t) ) {
				continue;
			}

			foreach( $s as $subKey => $subject ) {
				
				$sName = $subject['subject_name'];
				$sNum = $subject['subject_number'];
				$sWday = $subject['subject_weekday'];
				$sParity = $subject['subject_parity'];
				$sBreak = $subject['subject_break'];
				$sType = $subject['subject_type'];

				foreach( $subject['subject_groups'] as $gNum => $group ) {
					
					if( $group['teacher_name'] !== $name ) {
						continue;
					}

					$audithory = $group['auditory_name'];
					$sGroup = $t['faculty'].$t['department']."-".$t['semester'].$t['group'];
					$ss = new Subject($sName, $sType, $sWday, $sParity, $sNum, $sBreak, $sGroup, $audithory);
					if( !isset($teacher) || empty($teacher) ) {
						$teacher = new Teacher( $group['teacher_name'], $t['timetable']['date_start'], $t['timetable']['date_end'] );
					}
					$teacher->addNewSubject( $ss );
				}
			}
		}
		apiAnswer( array("status" => OK, "timetable" => $teacher) );

	}

	public function getClassmateList() {
		// if( !isset($_POST['email']) || empty($_POST['email']) ) {
		// 	apiAnswer(array("status" => FAIL, "message" => "Не указан email"));
		// 	return;
		// }
		if ( !accessIdentify($_POST) ) {
			apiAnswer(array("status" => FAIL, "message" => "access denied"));
			return;
		}
		$user = mDB::getInstance()->find( "users", array("id" => (int)$_POST['uid']) );
		
		if( empty($user) ) {
			apiAnswer(array("status" => FAIL, "message" => "no user"));
			return;
		}

		$group = $user[0]['group'];
		$faculty = $user[0]['faculty'];
		$department = $user[0]['department'];
		$semester = $user[0]['semester'];
		//$postfix = $user[0]['postfix'];

		$classmateList = mDB::getInstance()->find( "users", array( "group" => $group, 'faculty' => $faculty, "department" => $department, "semester" => $semester ) );
		apiAnswer(array("status" => OK, "classmateList" => $classmateList));
	}

	public function transferHeadmanRights() {
		
		if( !isset($_POST['emailTo']) || empty($_POST['emailTo']) ) {
			apiAnswer(array("status" => FAIL, "message" => "no email"));
			return;
		}

		if( !accessIdentify($_POST) ) {
			apiAnswer(array("status" => FAIL, "message" => "access denied"));
			return;
		}

		$user = mDB::getInstance()->find( "users", array("id" => (int)$_POST['uid']) );
		if( empty($user) ) {
			apiAnswer(array("status" => FAIL, "message" => "no user"));
		}

		$group = $user[0]['group'];
		$faculty = $user[0]['faculty'];
		$department = $user[0]['department'];
		$semester = $user[0]['semester'];
		//$postfix = $user[0]['postfix'];

		$isHeadman = (bool)$user[0]['headman'];

		if( !$isHeadman ) {
			apiAnswer(array( "status" => FAIL, "message" => "Вы не можете передавать права старосты, так как у вас их нет! АЗАЗ!") );
			return;
		}

		if( mDB::getInstance()->isInCollection("users", array("email" => $_POST['emailTo'], "group" => $group, 'faculty' => $faculty, "department" => $department, "semester" => $semester)) ) {
			mDB::getInstance()->update("users", array("id" => (int)$_POST['uid']), array('$set' => array("headman" => false)) );
			mDB::getInstance()->update("users", array("email" => $_POST['emailTo']), array('$set' => array("headman" => true)) );
			apiAnswer( array("status" => OK) );
		} else {
			apiAnswer( array("status" => FAIL, "message" => "такого пользователя нет"));
		}
	}
}

?>
