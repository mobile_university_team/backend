<?php

class Timetable extends apiAction {
	
	public function action($action) {

		if( !isset($action[1]) ) {
			throw new apiError(400, "Вы не указали действие профиля");
		}

		$this->$action[1]();
	}

	public function __call($name, $arguments) {
		throw new apiError(404, "Такого действия не существует");
	}

	private function get() {

		if( !isset($_GET['uid']) ) {
			apiAnswer(array("status" => FAIL, "message" => "Не указан uid"));
			return;
		}

		$user = User::getUser($_GET['uid']);

		if( !$user ) {
			apiAnswer(array("status" => FAIL, "message" => "Пользователя с таким id нет"));
			return;
		}

		$faculty = $user['faculty'];
		$department = $user['department'];
		$semester = $user['semester'];
		$group = $user['group'];

		$timetable = mDB::getInstance()->find("groupsTimetable", array( "group" => $group, 'faculty' => $faculty, "department" => $department, "semester" => $semester ));
		
		if( empty($timetable) ) {
			apiAnswer(array("status" => "no timetable"));
			return;
		}
		
		if( !isset($timetable[0]['timetable']) || is_null($timetable[0]['timetable'])) {
			apiAnswer(array("status" => "no timetable"));
			return;
		}
		
		apiAnswer(array("status" => OK, "timetable" => $timetable[0]['timetable']));
	}

	private function set() {
		if( !accessIdentify($_POST) ) {
			throw new apiError(403, "access denied");
		}
		if( isset($_POST['timetable']) ) {

			$_POST['timetable'] = json_decode($_POST['timetable']);
			if( isEmpty($_POST['timetable']) ) {
				apiAnswer(array("status" => "null timetable"));
				return;
			}
		} else {
			apiAnswer(array("status" => "null timetable"));
			return;
		}

		$uid = $_POST['uid'];

		if( User::isHeadman($uid) ) {

			$user = User::getUser($_POST['uid']);

			if( !$user ) {
				apiAnswer(array("status" => FAIL));
				return;
			}

			$faculty = $user['faculty'];
			$department = $user['department'];
			$semester = $user['semester'];
			$group = $user['group'];

			if( !mDB::getInstance()->isInCollection("groupsTimetable", array( "group" => $group, 'faculty' => $faculty, "department" => $department, "semester" => $semester )) ) {
				mDB::getInstance()->insert( "groupsTimetable", array("timetable" => $_POST['timetable'], "group" => $group, 'faculty' => $faculty, "department" => $department, "semester" => $semester) );
				apiAnswer( array("status" => OK) );
				return;
			}
			mDB::getInstance()->update( "groupsTimetable", array( "group" => $group, 'faculty' => $faculty, "department" => $department, "semester" => $semester ), array('$set' => array("timetable" => $_POST['timetable'])) );
			apiAnswer( array("status" => OK) );
		} else {
			apiAnswer( array("status" => "not headman") );
		}
	}
}

?>