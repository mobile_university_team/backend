<?php

class Comment extends apiAction {

	// private $category;

	public function action($action) {
		
		// if( count($action) == 2) {
			$this->$action[1]();
			return;
		// }
		// if( count($action) > 2 ) {

		// 	$category = $this->createSectionClassName($action[1]);
			
		// 	if( class_exists($category) ) {

		// 		$this->category = new $category();

		// 		if( isset($action[2]) ) {
		// 			if( method_exists($this->category, $action[2]) ) {
		// 				$this->category->$action[2]();
		// 			} else {
		// 				throw new apiError(404, "Такого действия не существует");
		// 			}
		// 		} else {
		// 			throw new apiError(400, "Вы не указали действие");
		// 		}
		// 	} else {
		// 		throw new apiError(400, "Вы не указали раздел");
		// 	}
		// }
	}

	public function __call($name, $args) {
		throw new apiError(404, "Действия $name не существует");
	}

	public function getAll() {
		if( empty($_GET['collection']) ) {
			throw new apiError(400, "Вы забыли указать раздел");
		}
		if( empty($_GET['objId']) ) {
			throw new apiError(400, "Вы забыли указать параметр objId");
		}

		$comments;
		$limit;
		$offset;

		if( !empty($_GET['limit']) ) {
			$limit = (int)$_GET['limit'];
		} else {
			$limit = null;
		}
		
		if( !empty($_GET['offset']) ) {
			$offset = (int)$_GET['offset'];
		} else {
			$offset = null;
		}

		$comments = mDB::getInstance()->find( "comments", array("collection" => $_GET['collection'], "objId" => $_GET['objId']), $limit, "date", $offset);
		$num = mDB::getInstance()->getCount("comments", array("collection" => $_GET['collection'], "objId" => $_GET['objId']));
		
		foreach( $comments as $n => $comment ) {

			$p = User::getUser($comment['personalUid']);
			if( !$p ) {
				continue;
			}

			$comments[$n]['profile'] = array( 
				"firstName" => $p['firstName'],
				"secondName" => $p['secondName'],
				"gender" => $p['gender'],
				"uid" => $p['id']
			);
			// $params = array( 
			// 	"firstName" => $p->firstName,
			// 	"secondName" => $p->secondName,
			// 	"birthday" => $p->birthday,
			// 	"gender" => $p->gender
			// );
			// $prof = new Profile($params);
			// $prof = (array) $prof;
			// $comments[$num][''];
		}
		apiAnswer(array("status" => OK, "comments" => $comments, "num" => $num));
	}

	public function get() {

		$cid = request::getQuery();
		$cid = $cid[2];

		if( empty($cid) ) {
			throw new apiError(400, "Вы забыли указать cid");
		}
		if( $cid === "all" ) {
			$this->getAll();
			return;
		}
		
		$comment = mDB::getInstance()->find( "comments", array("cid" => $cid) );
		
		$id = $comment[0]->personalUid;
		$user = User::getUser($id);
		
		$comment[0]['profile'] = array( 
			"firstName" => $user['firstName'],
			"secondName" => $user['secondName'],
			"gender" => $user['gender']
		);
		
		apiAnswer(array('status' => OK, "comment" => $comment));
	}

	public function add() {

		if( !accessIdentify($_POST) ) {
			throw new apiError(403, "access denied");
		}
		if( empty($_POST['text']) ) {
			throw new apiError(400, "Не передан текст комментария");
		}
		if( empty($_POST['collection']) ) {
			throw new apiError(400, "Не указан раздел комментария");
		}
		if( empty($_POST['objId']) ) {
			throw new apiError(400, "Не передан параметр objId");
		}

		$c = array( 
			"uid" => $_POST['uid'],
			"text" => $_POST['text'],
			"collection" => $_POST['collection'],
			"objId" => $_POST['objId']
		);

		$comment = new SubjectComment($c);
		$comment = (array)$comment;

		mDB::getInstance()->insert("comments", $comment);
		apiAnswer(array("status" => OK, "message" => "Комментарий добавлен", "cid" => $comment['cid']));
	}

	public function complain() {
		
		if( !accessIdentify($_POST) ) {
			throw new apiError(403, "access denied");
		}
		if( empty($_POST['uid']) ) {
			throw new apiError(400, "Не указан параметр uid");
		}
		if( empty($_POST['cid']) ) {
			throw new apiError(400, "Не указан параметр cid");
		}

		$alreadyVote = mDB::getInstance()->isInCollection("comments", array("complained.uid" => $_POST['uid']));
		
		if( $alreadyVote ) {
			throw new apiError(400, "Вы уже пожаловались раннее");
		}

		mDB::getInstance()->update("comments", array("cid" => $_POST['cid']), array('$push' => array("complained" => array("uid" => $_POST['uid']))));
		apiAnswer(array("status" => OK, "message" => "Ваша жалоба принята"));
	}

	public function update() {
		if( !accessIdentify($_POST) ) {
			throw new apiError(403, "access denied");
		}
		if( empty($_POST['cid']) ) {
			throw new apiError(400, "Не передан cid комментария");
		}
		if( empty($_POST['text']) ) {
			throw new apiError(400, "Не передан текст комментария");
		}

		mDB::getInstance()->update("comments", array("personalUid" => $_POST['uid'], "cid" => $_POST['cid']), array('$set' => array("text" => $_POST['text'])) );
		apiAnswer(array("status" => OK, "message" => "Комментарий обновлен"));
	}

	public function remove() {
		if( !accessIdentify($_POST) ) {
			throw new apiError(403, "access denied");
		}
		if( empty($_POST['cid']) ) {
			throw new apiError(400, "Не передан cid комментария");
		}

		mDB::getInstance()->remove("comments", array("personalUid" => $_POST['uid'], "cid" => $_POST['cid']));
		apiAnswer(array("status" => OK, "message" => "Комментарий удален"));
	}
}

class SubjectComment {

	public function __construct($vars) {
		$time = time();
		$this->cid = hash( "sha512", $vars['uid'].$time );
		$this->date = $time;
		$this->personalUid = $vars['uid'];
		$this->text = $vars['text'];
		$this->collection = $vars['collection'];
		$this->objId = $vars['objId'];
		$this->complained = array();
	}

	public $cid;
	public $date;
	public $personalUid;
	public $text;
	public $collection;
	public $objId;
	public $complained;

}

// class Profile {

// 	public function __construct($params) {
// 		$this->firstName = $params['name'];
// 		$this->gender = $params['gender'];
// 		$this->birthday = $params['birthday'];
// 		$this->secondName = $params['secondName'];
// 	}

// 	public $firstName;
// 	public $secondName;
// 	public $gender;
// 	public $birthday;

// }
?>
