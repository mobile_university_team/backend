<?php

class Profile extends apiAction {

	public function action($action) {
		$this->$action[1]();
	}

	private function get() {
		if( accessIdentify($_GET) ) {
			$profile = User::getUser($_GET['uid']);
			apiAnswer(array("status" => OK, "profile" => $profile));
		} else {
			apiAnswer(array("status" => "access denied"));
		}
	}

	public function __call($name, $args) {
		throw new apiError(404, "Такого действия у профиля не существует");
	}
}

?>