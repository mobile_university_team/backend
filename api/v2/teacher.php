<?php

class Teacher extends apiAction {
	
	public function action($action) {
		
		if( isset($action[1]) ) {

			$section = $this->createSectionClassName($action[1]);

			if( class_exists($section) ) {

				$this->section = new $section();
				
				if( isset($action[2]) ) {

					if( method_exists($this->section, $action[2]) ) {
						$this->section->$action[2]();
					} else {
						throw new apiError(404, "Такого действия не существует");
					}
				} else {
					throw new apiError(400, "Вы не указали действие");
				}
			} else {
				throw new apiError(404, "Вы указали неверный раздел");
			}
		} else {
			throw new apiError(400, "Вы не указали раздел");
		}

	}
};

class TeacherList {

	public function get($return = false) {

		$timetables = mDB::getInstance()->find( "groupsTimetable", array());
		$teacherList = array();

		foreach( $timetables as $key => $t ) {
			
			if( empty($t['timetable']) ) {
				continue;
			}

			$t = $t['timetable']['subjects'];

			if( empty($t) ) {
				continue;
			}
			
			foreach( $t as $subKey => $subject ) {
				foreach( $subject['subject_groups'] as $gNum => $group ) {
					if( !in_array($group['teacher_name'], $teacherList) && $group['teacher_name'] != "" ) {
						$teacherList[] = $group['teacher_name'];
					}
				}
			}
		}

		if( $return ) {
			return $teacherList;
		}

		apiAnswer( array("status" => OK, "teachers" => $teacherList));
	}


};

class TeacherTimetable {

	public function get() {

		$name;
		$teacher;
		
		if( isset($_GET['name']) && !empty($_GET['name']) ) {
			$name = $_GET['name'];
		} else {
			apiAnswer( array("status" => FAIL, "message" => "Не указано имя преподавателя") );
			return;
		}

		$timetables = mDB::getInstance()->find( "groupsTimetable", array("timetable.subjects.subject_groups.teacher_name" => $name) );
		if( empty($timetables) ) {
			apiAnswer(array("status" => FAIL, "message" => "no teacher"));
			return;
		}

		foreach( $timetables as $key => $t ) {
			$s = $t['timetable']['subjects'];
			if( empty($t) ) {
				continue;
			}

			foreach( $s as $subKey => $subject ) {
				
				$sName = $subject['subject_name'];
				$sNum = $subject['subject_number'];
				$sWday = $subject['subject_weekday'];
				$sParity = $subject['subject_parity'];
				$sBreak = $subject['subject_break'];
				$sType = $subject['subject_type'];

				foreach( $subject['subject_groups'] as $gNum => $group ) {
					
					if( $group['teacher_name'] !== $name ) {
						continue;
					}

					$audithory = $group['auditory_name'];
					$sGroup = $t['faculty'].$t['department']."-".$t['semester'].$t['group'];
					$ss = new Subject($sName, $sType, $sWday, $sParity, $sNum, $sBreak, $sGroup, $audithory);
					if( !isset($teacher) || empty($teacher) ) {
						$teacher = new Teacher2( $group['teacher_name'], $t['timetable']['date_start'], $t['timetable']['date_end'] );
					}
					$teacher->addNewSubject( $ss );
				}
			}
		}
		apiAnswer( array("status" => OK, "timetable" => $teacher) );

	}
};

class Subject {
	
	public function __construct($name, $type, $weekday, $parity, $number, $break, $group_name, $auditory) {
		$this->subject_name = $name;
		$this->subject_type = $type;
		$this->subject_weekday = $weekday;
		$this->subject_parity = $parity;
		$this->subject_number = $number;
		$this->subject_break = $break;
		$this->subject_group_name = $group_name;
		$this->auditory_name = $auditory;
	}

	public $subject_name;
	public $subject_type;
	public $subject_weekday;
	public $subject_parity;
	public $subject_number;
	public $subject_break;
	public $subject_group_name;
	public $auditory_name;
}

class TeacherRating {

	public function get() {

		$limit = null;

		if( !empty($_GET['limit']) ) {
			$limit = (int) $_GET['limit'];
		}
		$teachers = new TeacherList();
		$teachers = $teachers->get(true);

		var_dump($teachers);
		
		$likes = mDB::getInstance()->find( "likes", array("collecion" => "teachers") );

	}
}

class Teacher2 {

	public function __construct($name, $date_start, $date_end) {
		$this->name = $name;
		$this->date_start = $date_start;
		$this->date_end = $date_end;
		$this->subjects = array();
	}

	public function addNewSubject($subject) {
		if( !($subject instanceof Subject) ) {
			return;
		}
		if( !$this->isInList($subject) ) {
			$this->subjects[] = $subject;
		}
	}

	public function isInList($subject) {
		foreach( $this->subjects as $key => $sub ) {
			if( $subject->subject_name === $sub->subject_name &&
				$subject->subject_number === $sub->subject_number &&
				$subject->subject_weekday === $sub->subject_weekday
				/* $subject->subject_type === $sub->subject_type */ ) {
				return true;
			}
		}
		return false;
	}

	public $name;
	public $subjects;
	public $date_start;
	public $date_end;

}
?>