<?php

class Classmate extends apiAction {

	public function action($action) {
		
		if( count($action) == 2 ) {
			$this->$action[1]();
			return;
		}

		if( count($action) > 2 ) {

			$section = $this->createSectionClassName($action[1]);
			
			if( class_exists($section) ) {

				$this->section = new $section();

				if( isset($action[2]) ) {

					if( method_exists($this->section, $action[2]) ) {
						$this->section->$action[2]();
					} else {
						throw new apiError(404, "Такого действия не существует");
					}

				} else {
					throw new apiError(400, "Вы не указали действие");
				}

			} else {
				throw new apiError(400, "Вы не указали раздел");
			}

		}

	}

	public function __call($name, $args) {
		throw new apiError(404, "Действия $name не существует");
	}

	private function transferRights() {
		if( !isset($_POST['emailTo']) || empty($_POST['emailTo']) ) {
			apiAnswer(array("status" => FAIL, "message" => "no email"));
			return;
		}

		if( !accessIdentify($_POST) ) {
			apiAnswer(array("status" => FAIL, "message" => "access denied"));
			return;
		}

		$user = mDB::getInstance()->find( "users", array("id" => (int)$_POST['uid']) );
		if( empty($user) ) {
			apiAnswer(array("status" => FAIL, "message" => "no user"));
		}

		$group = $user[0]['group'];
		$faculty = $user[0]['faculty'];
		$department = $user[0]['department'];
		$semester = $user[0]['semester'];
		//$postfix = $user[0]['postfix'];

		$isHeadman = (bool)$user[0]['headman'];

		if( !$isHeadman ) {
			apiAnswer(array( "status" => FAIL, "message" => "Вы не можете передавать права старосты, так как у вас их нет! АЗАЗ!") );
			return;
		}

		if( mDB::getInstance()->isInCollection("users", array("email" => $_POST['emailTo'], "group" => $group, 'faculty' => $faculty, "department" => $department, "semester" => $semester)) ) {
			mDB::getInstance()->update("users", array("id" => (int)$_POST['uid']), array('$set' => array("headman" => false)) );
			mDB::getInstance()->update("users", array("email" => $_POST['emailTo']), array('$set' => array("headman" => true)) );
			apiAnswer( array("status" => OK) );
		} else {
			apiAnswer( array("status" => FAIL, "message" => "такого пользователя нет"));
		}
	}
}

class ClassmateList {

	public function get() {

		if( !isset($_GET['uid']) || empty($_GET['uid']) ) {
			apiAnswer(array("status" => FAIL, "message" => "Не указан uid"));
			return;
		}
		// if ( !accessIdentify($_POST) ) {
		// 	apiAnswer(array("status" => FAIL, "message" => "access denied"));
		// 	return;
		// }
		$user = mDB::getInstance()->find( "users", array("id" => (int)$_GET['uid']) );
		
		if( empty($user) ) {
			apiAnswer(array("status" => FAIL, "message" => "no user"));
			return;
		}

		$group = $user[0]['group'];
		$faculty = $user[0]['faculty'];
		$department = $user[0]['department'];
		$semester = $user[0]['semester'];
		//$postfix = $user[0]['postfix'];

		$classmateList = mDB::getInstance()->find( "users", array( "group" => $group, 'faculty' => $faculty, "department" => $department, "semester" => $semester ) );
		apiAnswer(array("status" => OK, "classmateList" => $classmateList));

	}
}

?>