<?php

class Key extends apiAction {
	
	public function action($action) {
		$this->$action[1]();
	}

	public function __call($name, $args) {
		throw new apiError(404, "Такого действия не существует");
	}

	public function get() {
		apiAnswer(array("key" => file_get_contents(__DIR__."/mobile_university_pub.pem")));
	}
}

?>