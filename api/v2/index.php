<?php

class Starter {
	
	public static function start() {
		
		$collection = request::getQuery()[0];
		
		if( empty($collection) ) {
			throw new apiError(400, "Вы не указали раздел api с которым работать");
		}
		
		$api = ApiCollectionCreator::createCollection($collection);
		$api->action(request::getQuery());
	}

}

abstract class apiAction {

	private $section;
	abstract public function action($action);

	public function createSectionClassName($sectionName) {
		
		if( empty($sectionName) ) {
			return "";
		}

		$sectionName = strtolower($sectionName);
		$sectionName[0] = strtoupper($sectionName[0]);
		return get_called_class().$sectionName;
	}
}

class ApiCollectionCreator {

	public static function createCollection($name) {

		if( file_exists(__DIR__."/".$name.".php") ) {

			include $name.".php";
			$collection = $name;

			$collection = strtolower($collection);
			$collection[0] = strtoupper($collection[0]);

			return new $collection();

		} else {
			throw new apiError(400, "Такого раздела api не существует");
		}
	}
}

define("FAIL", "failed");
define("OK", "ok");

function getDataFromEncryptedString($str) {
	$data = "";
	$str = base64_decode($str);
	$res = openssl_private_decrypt($str, $data, file_get_contents(__DIR__."/mobile_university_rsa_1024_private.pem"));
	
	if ( $res ) {
		return $data;
	} else {
		return false;
	}
}


function accessIdentify($data){
	if( !isset($data['uid']) || !isset($data['hash']) ) {
		return false;
	}
	$uid = $data['uid'];
	$hash = $data['hash'];

	unset($data['uid']);
	unset($data['hash']);


	// foreach($data as $key=>$val) {
	// 	$data[$key] = strtolower($val);
	// }

	// sort($data, SORT_STRING);
	
	
	$token = User::getToken($uid);
	if( $token === false ) {
		return false;
	}
	$token = $token['token'];
	
	$identifyStr = $uid;
	$identifyStr .= $token;
	// foreach($data as $key=>$val) {
	// 	$identifyStr .= $val;
	// }

	$ansHash = hash("sha512", $identifyStr);
	return $hash === $ansHash;
}

function needValueFromHeader( $type, $arrValues ) {
	foreach( $arrValues as $key => $val ) {
		if( empty($type[$val]) ) {
			throw new apiError(400, "Не передан параметр $val");
		}
	}
}
?>
