<?php

class Rights extends apiAction {
	
	public function action($action) {

		if( !isset($action[1]) ) {
			throw new apiError(400, "Вы не указали действие профиля");
		}
		
		$this->$action[1]();
	}

	private function get() {
		if( accessIdentify($_POST) ) {
			$rights = array(
				"admin" => User::isAdmin($_POST['uid']),
				"headman" => User::isHeadman($_POST['uid'])
			);
			apiAnswer($rights);
		} else {
			apiAnswer( array( 
				"admin" => false,
				"headman" => false) 
			);
		}
	}

	public function __call($name, $args) {
		throw new apiError(404, "Такого действия не существует");
	}
}

?>