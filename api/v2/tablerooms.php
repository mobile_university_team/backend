<?php

class Tablerooms extends apiAction {

	public function action( $action ) {
		$this->$action[1]();
	}

	public function vote() {

		if( !accessIdentify($_POST) ) {
			throw new apiError(403, "access denied");
		}
		if( !isset($_POST['starVal']) ) {
			throw new apiError(400, "Не указан параметр starVal");
		}
		if( empty($_POST['tableroomId']) ) {
			throw new apiError(400, "Не указан параметр tableroomId");
		}

		$starVal = (int) $_POST['starVal'];

		if( $starVal > 10 || $starVal < 0 ) {
			throw new apiError(400, "out of boundary");
		}
		
		// $lastVote = mDB::getInstance()->find( "tablerooms", array("tt_id" => $_POST['tableroomId']), 1, "time" );
		
		// if( empty($lastVote) ) {

			$vote = new Voting($_POST);
			mDB::getInstance()->insert( "tablerooms", (array) $vote );

		// } else {

			// $now = time();
			// $last = $lastVote[0]['time'];
			// if( $now - $last < 60*15 ) {

			// 	apiAnswer(array("status" => FAIL, "message" => "Вы можете голосовать раз в 15 минут"));
			// 	return;

			// } else {

				// $vote = new Voting($_POST);
				// mDB::getInstance()->insert( "tablerooms", (array) $vote );

			// }
		// }
		apiAnswer(array("status" => OK));
	}

	public function get() {

		needValueFromHeader( $_GET, array('from', 'to', "tableroomId") );

		$statistic = mDB::getInstance()->find( "tablerooms", array("tt_id" => $_GET['tableroomId'], "time" => array('$gt' => (int) $_GET['from'], '$lt' => (int) $_GET['to'])), null, "time" );

		$res = array();
		
		$floor = 0;
		$length = count($statistic);

		for( $i = 0; $i < $length; $i++ ) {

			if( $statistic[$floor]['time'] - $statistic[$i]['time'] <= 120 && ($i+1 != $length) ) {
				continue;
			}

			$res[] = new MidRating( array_slice($statistic, $floor, $i) );
			$floor = $i;
		}

		apiAnswer( array("status" => OK, "statistic" => $res) );
	}
}

class Voting {

	public function __construct($vars) {
		$this->star = (int) $vars['starVal'];
		$this->uid = $vars['uid'];
		$this->tt_id = $vars['tableroomId'];
		$this->time = time();
	}

	public $tt_id;
	public $star;
	public $uid;
	public $time;

}

class MidRating {

	public $midRating;
	public $midTime;

	public function __construct($ob) {
		$this->midRating = $this->getMid($ob, "star");
		$this->midTime = $this->getMid($ob, "time");
	}

	public function getMid($arr, $val) {
		$res = 0;
		foreach( $arr as $num => $v ) {
			$res += $v[$val];
		}

		if( count($arr) == 0 ) {
			return 0;
		}
		
		$res = $res/count($arr);
		return $res;
	}
}
?>