<?php

class Likes extends apiAction {

	// private $category;

	public function action($action) {
		
		// if( count($action) == 2) {
			$this->$action[1]();
			return;
		// }
		// if( count($action) > 2 ) {

		// 	$category = $this->createSectionClassName($action[1]);
			
		// 	if( class_exists($category) ) {

		// 		$this->category = new $category();

		// 		if( isset($action[2]) ) {
		// 			if( method_exists($this->category, $action[2]) ) {
		// 				$this->category->$action[2]();
		// 			} else {
		// 				throw new apiError(404, "Такого действия не существует");
		// 			}
		// 		} else {
		// 			throw new apiError(400, "Вы не указали действие");
		// 		}
		// 	} else {
		// 		throw new apiError(400, "Вы не указали раздел");
		// 	}
		// }
	}

	public function __call($name, $args) {
		throw new apiError(404, "Действия $name не существует");
	}

	public function set() {

		if( !accessIdentify($_POST) ) {
			throw new apiError(403, "access denied");
		}

		if( !isset($_POST['likeType']) ) {
			throw new apiError(400, "Не указан параметр likeType");
		}

		needValueFromHeader( $_POST, array("objId", "collection"));

		$_POST['likeType'] = (int) $_POST['likeType'];
		
		if( $_POST['likeType'] > 1 || $_POST['likeType'] < -1 ) {
			throw new apiError(400, "Параметр likeType выходит за пределы");
		}

		$haveLike = mDB::getInstance()->isInCollection("likes", array("objId" => $_POST['objId'], "collection" => $_POST['collection']));
		$like = new PersonalLike($_POST);
		$_POST['like'] = $like;

		if( !$haveLike ) {
			$like = new LikeSubject($_POST);
			$like = (array) $like;
			mDB::getInstance()->insert("likes", $like);
		} else {
			mDB::getInstance()->update("likes", array("collection" => $_POST['collection'], "objId" => $_POST['objId']), array('$pull' => array("personalUidList" => array("uid" => $_POST['uid']))) );
			mDB::getInstance()->update("likes", array("collection" => $_POST['collection'], "objId" => $_POST['objId']), array('$push' => array("personalUidList" => (array) $like )));
		}

		apiAnswer(array("status" => OK));
	}

	public function get() {
		
		needValueFromHeader( $_GET, array("objId", "collection", "uid") );

		$likes = mDB::getInstance()->find("likes", array("objId" => $_GET['objId'], "collection" => $_GET['collection']));
		
		$NumOfLikes = 0;
		$NumOfDislikes = 0;

		if( empty($likes[0]) ) {
			apiAnswer( array("status" => OK, "likes" => null, "NumOfLikes" => 0, "NumOfDislikes" => 0, "likeTypeForCurrentUser" => 0) );
			return;
		}

		$likeTypeForCurrentUser = 0;

		foreach( $likes[0]['personalUidList'] as $n => $l ) {
			if( $l['likeType'] == 1 ) {
				$NumOfLikes++;
			}
			if( $l['likeType'] == -1 ) {
				$NumOfDislikes++;
			}
			if( $l['uid'] == $_GET['uid'] ) {
				$likeTypeForCurrentUser = $l['likeType'];
			}
		}

		apiAnswer( array("status" => OK, "likes" => $likes[0], "NumOfLikes" => $NumOfLikes, "NumOfDislikes" => $NumOfDislikes, "likeTypeForCurrentUser" => $likeTypeForCurrentUser) );
	}

	// public function unlike() {

	// 	if( !accessIdentify($_POST) ) {
	// 		throw new apiError(403, "access denied");
	// 	}
	// 	if( empty($_POST['objId']) ) {
	// 		throw new apiError(400, "Не указан параметр objId");
	// 	}

	// 	mDB::getInstance()->update("likes", array("objId" => $_POST['objId']), array('$pull' => array("personalUidList" => array($_POST['uid']))) );
	// 	apiAnswer(array("status" => OK));

	// }

	// public function dislike() {

	// 	if( !accessIdentify($_POST) ) {
	// 		throw new apiError(403, "access denied");
	// 	}
	// 	if( empty($_POST['objId']) ) {
	// 		throw new apiError(400, "Не указан параметр objId");
	// 	}

	// 	$haveDisLike = mDB::getInstance()->isInCollection("dislikes", array("objId" => $_POST['objId']));
		
	// 	if( !$haveDisLike ) {
	// 		$like = new LikeSubject($_POST);
	// 		$like = (array) $like;
	// 		mDB::getInstance()->insert("likes", $like);
	// 	} else {
	// 		mDB::getInstance()->update("likes", array("objId" => $_POST['objId']), array('$push' => array("personalUidList" => $_POST['uid'])) );
	// 	}

	// 	apiAnswer(array("status" => OK));

	// }

	// public function undislike() {

	// }
}

class LikeSubject {

	public function __construct($vars) {
		$this->personalUidList = array($vars['like']);
		$this->objId = $vars['objId'];
		$this->collection = $vars['collection'];
	}

	public $personalUidList;
	public $objId;
	public $collection;

}

class PersonalLike {
	
	public function __construct($vars) {
		$this->likeType = (int) $vars['likeType'];
		$this->uid =$vars['uid'];
	}

	public $likeType;
	public $uid;

}
?>