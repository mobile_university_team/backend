<?php

class auth extends apiAction{
	
	public function action($action) {

		if( !isset($action[1]) ) {
			throw new apiError(400, "Вы не указали действие");
		}

		$this->$action[1]();
	}
	
	public function __call($name, $args) {
			throw new apiError(404, "Действия $name не существует");
	}

	public function __construct() {}

	private function auth() {
		if( !isset($_POST['data']) || empty($_POST['data']) ) {
			apiAnswer(array("status" => "no data"));
			return;
		}

		$data = getDataFromEncryptedString( $_POST['data'] );

		if( !$data ) {
			apiAnswer(array("status" => "data corrupted"));
			return;
		}

		$_POST = json_decode($data, true);

		$email = "";
		$pass = "";

		if( isset($_POST['email']) && !empty($_POST['email']) ) {
			$email = $_POST['email'];
		} else {
			apiAnswer( array("status" => FAIL, "message" => "Укажите email") );
			return;
		}

		if( isset($_POST['pass']) && !empty($_POST['pass']) ) {
			$pass = hash("sha512", $_POST['pass']);
		} else {
			apiAnswer( array("status" => FAIL, "message" => "Укажите пароль") );
			return;
		}

		$query = array("email" => $email, "pass" => $pass);

		try {
			$user = mDB::getInstance()->find("users", $query);
		} catch( Exception $e) {
			putLog( LOG_PATH, $e->getMessage() );
			die();
		}

		if( !empty($user) ) {
			$user = $user[0];
			if( !empty($user['token']) ) {
				apiAnswer( array("status" => OK, "token" => $user['token']['token'], "uid" => $user['id']) );
			} else {
				$time = time();
				$timeTo = $time+(3600*3600*3600);
				$token = hash("sha512", $email.$pass.$time);
				$uid = User::setUser( $email, $token, $timeTo );
				apiAnswer( array("status" => OK, "token" => $token, "uid" => $uid) );
			}
		} else {
			apiAnswer( array("status" => FAIL, "message" => "Такого пользователя не существует") );
		}
	}

	private function register() {
		// if( !isset($_POST['data']) || empty($_POST['data']) ) {
		// 	apiAnswer(array("status" => "no data"));
		// 	return;
		// }

		// $data = getDataFromEncryptedString( $_POST['data'] );

		// if( !$data ) {
		// 	apiAnswer(array("status" => "data corrupted"));
		// 	return;
		// }
		
		// $_POST = json_decode($data, true);
		
		if( isset($_POST['headman']) && $_POST['headman'] !== false && $_POST['headman'] !== "false" ) {
			$headmanIsAlreadyExists = mDB::getInstance()->isInCollection( "users", array("group" => $_POST['group'], "faculty" => $_POST['faculty'], "department" => $_POST['department'], "semester" => $_POST['semester'], "headman" => true) );
			if( $headmanIsAlreadyExists ) {
				apiAnswer(array("status" => FAIL, "message" => "Регистрация прервана! В вашей группе уже есть староста! Обратитесь к нему, чтобы он передал вам полномочия") );
				return;
			} else {
				$_POST['headman'] = true;
			}
		}
		
		// if( empty($_POST['postfix']) ) {
			// unset( $_POST['postfix'] );
		// }

		if( !isset($_POST['email']) || empty($_POST['email']) ) {
			apiAnswer( array("status" => FAIL, "message" => "Не указан email") );
			return;
		}
		
		if( !preg_match('/.+@.+\..+/', $_POST['email']) ) {
			apiAnswer(array("status" => FAIL, "message" => "мыло не валидно!"));
			return;
		}

		if( !isset($_POST['pass']) || empty($_POST['pass']) ) {
			apiAnswer( array("status" => FAIL, "message" => "Укажите пароль") );
			return;
		}
		
		foreach( $_POST as $key => $val) {
			if( empty($val) ) {
				apiAnswer(array("status" => FAIL, "message" => "Одно из полей пропущено") );
				return;
			}
		}
		

		if( mDB::getInstance()->isInCollection("users", array("email" => $_POST['email'])) ) {
			apiAnswer(array("status" => FAIL, "message" => "Такой пользователь уже существует") );
			return;
		}
		
		$_POST['pass'] = hash("sha512", $_POST['pass']);
		$_POST['rights'] = "reader";

		try {
			$res = mDB::getInstance()->insert("users", $_POST);
		} catch( Exception $e ) {
			putLog( LOG_PATH, $e->getMessage() );
			die();
		}
		mail($_POST['email'], "Спасибо за регистрацию", "Спасибо что решили воспользоваться нашим приложением! Надеемся вам понравится!", 'From: e-university@tp-dev.com'."\r\n" . 'Content-type: text/html; charset=utf8'."\r\n");
		apiAnswer( array("status" => OK) );
	}

	private function logged() {
		if( accessIdentify($_GET) ) {
			$uid = $_GET['uid'];
			$hash = $_GET['hash'];
			$valid = User::isValidToken($uid);
			if( $valid ) {
				apiAnswer(array("logged" => true));
			} else {
				apiAnswer(array("logged" => false));
			}
		} else {
			apiAnswer(array("logged" => false));
		}
	}

}

?>