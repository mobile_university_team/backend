<?php

class Organizations extends apiAction {

	public function action($action) {

		$section = $this->createSectionClassName($action[1]);

		if( class_exists( $section ) ) {
			$this->section = new $section();

			if( isset($action[2]) ) {

				if( method_exists($this->section, $action[2]) ) {
					$this->section->$action[2]();
				} else {
					throw new apiError(404, "Такого действия не существует");
				}

			} else {
				throw new apiError(400, "Вы не указали действие");
			}

		} else {
			throw new apiError(400, "Вы указали не правильный раздел");
		}

	}

}

class OrganizationsDb {

	private static $version = 2;

	public function get( $file = "orgDb.db" ) {
		if (ob_get_level()) {
	      ob_end_clean();
	    }
	    header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename='.basename($file));
	    header('Content-Transfer-Encoding: binary');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($file));
	    readfile($file);
	    exit;
	}

	public function version() {

		$query = request::getQuery();

		if( !isset($query[3]) ) {
			throw new apiError(400, "Вы не указали действие");
		}

		switch( request::getQuery()[3] ) {
			case "get" : apiAnswer(array("status" => OK, "version" => self::$version)); break;
			default: throw new apiError(404, "Такого действия не существует");
		}
	}
	
}

?>