<?php


class controller_admin extends controller {

	public function init() {
		if( isAdmin() ) {
			generateView("admin_panel");
		} else {
			header("Location: /auth");
		}
	}

	public function addUser() {
		try {
			if( isAdmin() ) {
				$data = (array) json_decode( $_POST['data'] );
				
				$this->model->addUser($data);

				$ans = array("status" => "OK");
				echo json_encode( $ans );
				
			} else {
				$ans = array("status" => "Error", "errorText" => "no Permissions");
				echo json_encode( $ans );
			}
		} catch ( Exception $e ) {
			$err = array(
				"status" => "Error",
				"errorText" => $e->getMessage()
			);
			echo json_encode($err);
		}
	}

	public function addOrganization() {
		try {
			if( isAdmin() ) {
				$data = (array) json_decode( $_POST['data'] );
				$this->model->addOrganization($data);

				$ans = array("status" => "OK");
				echo json_encode( $ans );

			} else {
				$ans = array("status" => "Error", "errorText" => "no Permissions");
				echo json_encode( $ans );
			}

		} catch ( Exception $e ) {
			$err = array(
				"status" => "Error",
				"errorText" => $e->getMessage()
			);
			echo json_encode($err);
		} 
	}

	public function editOrganization() {

	}

	public function removeOrganization() {

	}

	public function getOrganizationParams($id) {
		
	}
}

function addPanel($name) {
	$file = __DIR__."/adminPanelModules/".$name.".php";
	if( file_exists($file) ) {
		include $file;
	}
}

?>
