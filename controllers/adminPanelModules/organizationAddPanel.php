<form name="organizationAddForm" class="addForm emerald">
	<b>Добавить организацию</b>
	<table>

		<tr>
			<td>Название </td>
			<td><input type="text" name="name" required maxlength="250"></td>
		</tr>
	
		<tr>
			<td>Аудитория </td>
			<td><input type="text" name="aud" required maxlength="10"></td>
		</tr>

		<tr>
			<td>Часы приема: <input type="checkbox" name="hasTime"> </td>
			<td>
				<table id="timetable" hidden>
					<tr>
						<td>Понедельник:</td>
						<td> c <input type="time" name="monFrom"> до <input type="time" name="monTo"> </td>
					</tr>
					<tr>
						<td>Вторник: </td>
						<td> c <input type="time" name="tuFrom"> до <input type="time" name="tuTo"> </td>
					</tr>
					<tr>
						<td>Среда: </td>
						<td> c <input type="time" name="wedFrom"> до <input type="time" name="wedTo"> </td>
					</tr>
					<tr>
						<td>Четверг: </td>
						<td> c <input type="time" name="thuFrom"> до <input type="time" name="thuTo"> </td>
					</tr>
					<tr>
						<td>Пятница: </td>
						<td> c <input type="time" name="friFrom"> до <input type="time" name="friTo"> </td>
					</tr>
					<tr>
						<td>Суббота: </td>
						<td> c <input type="time" name="sutFrom"> до <input type="time" name="sutTo"> </td>
					</tr>
					<tr>
						<td>Воскресенье: </td>
						<td> c <input type="time" name="sunFrom"> до <input type="time" name="sunTo"> </td>
					</tr>
				</table>
			</td>
		</tr>

	</table>
	<input type="submit" value="Добавить" id="organizationAdd">
</form>

<script>
$(function() {

$("input[name='hasTime']").on("change", function(e) {
	$("#timetable").toggle();
});

$("form[name='organizationAddForm']").on("submit", function(e) {

	e.preventDefault();

	var data = {
		orgInfo : {}
	};
	var timetable = {};
	var formElemList = document.forms.organizationAddForm.elements;
	
	data.orgInfo["name"] = formElemList.name.value;
	data.orgInfo["aud"] = formElemList.aud.value;
	
	if( formElemList.hasTime.checked ) {
		
		timetable.monday = {timeFrom : formElemList.monFrom.value, timeTo : formElemList.monTo.value};
		timetable.tuesday = {timeFrom : formElemList.tuFrom.value, timeTo : formElemList.tuTo.value};
		timetable.wednesday = {timeFrom : formElemList.wedFrom.value, timeTo : formElemList.wedTo.value};
		timetable.thursday = {timeFrom : formElemList.thuFrom.value, timeTo : formElemList.thuTo.value};
		timetable.friday = {timeFrom : formElemList.friFrom.value, timeTo : formElemList.friTo.value};
		timetable.suturday = {timeFrom : formElemList.sutFrom.value, timeTo : formElemList.sutTo.value};
		timetable.sunday = {timeFrom : formElemList.sunFrom.value, timeTo : formElemList.sunTo.value};

		data['timetable'] = timetable;
	}
	data = JSON.stringify(data);

	$.ajax({
		url : "/admin/addOrganization",
		data : "data="+data,
		type : "POST",
		success : function(msg) {
			console.log(msg);
			try {
				msg = JSON.parse(msg);
				if (msg.status == "Error") {
					createMessage("error", msg.errorText);
				}
				if (msg.status == "OK") {
					createMessage("ok", "Good!");
					for(var key in formElemList) {
						formElemList[key].value = '';
						$("#organizationAdd").val("Добавить");
					}
				}
			} catch( e ) {
				createMessage("error", "Какая-то ошибочка");

			}
			

		}
	})

});
});
</script>