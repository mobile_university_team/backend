<form name="userAddForm" class="addForm tortoise" method="POST">
	<b>Добавить тестового студента</b>
	<table>

		<tr>
			<td>Email</td>
			<td><input type="text" name="email"></td>
		</tr>

		<tr>
			<td>Имя</td>
			<td><input type="text" name="firstName"></td>
		</tr>
		
		<tr>
			<td>Фамилия</td>
			<td><input type="text" name="secondName"></td>
		</tr>

		<tr>
			<td>Дата рождения</td>
			<td><input type="date" name="birthday"></td>
		</tr>

		<tr>
			<td>Пол</td>
			<td>
				Мужской <input type="radio" name="gender" value="male"><br>
				Женский <input type="radio" name="gender" value="female">
			</td>
		</tr>

		<tr>
			<td>Группа</td>
			<td>
				<select name="group">
					<option value="ИУ5-22">ИУ5-22</option>
					<option value="ИУ5-24">ИУ5-24</option>
					<option value="ИУ4-12">ИУ4-12</option>
					<option value="СМ3-33">СМ3-33</option>
					<option value="ИБМ1-45">ИБМ1-45</option>
					<option value="РК3-31">РК3-31</option>
					<option value="РЛ5-46">РЛ5-46</option>
				</select>
			</td>
		</tr>

		<tr>
			<td>Полномочия</td>
			<td>
				<select name="rights">
					<option value="reader">reader</option>
					<option value="admin">admin</option>
					<option value="moderator">moderator</option>
				</select>
			</td>
		</tr>

		<tr>
			<td> Является старостой </td>
			<td> <input type="checkbox" name="headman"> </td>
		</tr>


		<tr>
			<td>Логин электронного университета</td>
			<td><input type="text" name="loginBmstu"></td>
		</tr>

		<tr>
			<td>Пароль</td>
			<td><input type="password" name="pass"></td>
		</tr>

	</table>
	<input type="submit" value="Добавить" id="userAdd">
</form>

<script>
$("#userAdd").on("click", function(e) {

	var data = {};
	var formElemList = document.forms.userAddForm.elements;
	
	data["email"] = formElemList.email.value;
	data["firstName"] = formElemList.firstName.value;
	data["secondName"] = formElemList.secondName.value;
	data["gender"] = formElemList.gender.value;
	data["group"] = formElemList.group.value;
	data["loginBmstu"] = formElemList.loginBmstu.value;
	data["pass"] = formElemList.pass.value;
	data["birthday"] = formElemList.birthday.value;
	data['rights'] = formElemList.rights.value;
	data['headman'] = formElemList.headman.checked;
	
	data = JSON.stringify(data);

	$.ajax({
		url : "/admin/addUser",
		data : "data="+data,
		type : "POST",
		success : function(msg) {
			console.log(msg);
			try {
				msg = JSON.parse(msg);
				if (msg.status == "Error") {
					createMessage("error", msg.errorText);
				}
				if (msg.status == "OK") {
					createMessage("ok", "Good!");
				}
			} catch( e ) {
				createMessage("error", "Какая-то ошибочка");
			}
		}
	});

	e.preventDefault();
});

$("input[name='hasTime'").on("change", function(e){
	$("#timetable").toggle();
});
</script>