<?php

class controller_profile extends controller {

	public function init() {
		if( User::isLogged() ) {
			$classmates = $this->model->getClassmates();
			$this->view->generateProfilePage( array("classmates" => $classmates, "editInformation" => null) );
		} else {
			$this->view->generateUnautorizedUserPage();
		}
	}
}

?>