<?php

class controller_timetable extends controller{

	public function init() {
		if( isLogged() ) {
			$this->createTimetable();
		} else {
			$this->view->showMessage("Вы не авторизованы", "error");
		}
	}

	public function createTimetable() {
		try{
				include __DIR__."/../templates/timetable/tteditor/index.html";
		} catch( Exception $e ) {
			$message = $e->getMessage();
			$this->view->showMessage($message, "error");
		}
	}
	public function setTimetable() {

			$ans = apiRequest("setTimetable", "POST", array(
				"uid" => $_COOKIE['uid'],
				"hash" => hash("sha512", $_COOKIE['uid'].$_COOKIE['token'])
				)
			);
			switch( $ans['status'] ) {
				case 'ok' : $this->view->showMessage("Расписание успешно создано!", "ok"); break;
				case "not headman" : $this->view->showMessage("Нужно обладать правами старосты!", "error"); break;
				case "null timetable" : $this->view->showMessage("Расписание не заполнено", "error"); break;
				default: $this->view->showMessage("Ошибка!", "error"); break;
			}
	}

	public function getTimetable() {
		$hash = hash("sha512", $_COOKIE['uid'].$_COOKIE['token']);
		
		$timetable = apiRequest("getTimetable", "POST", array(
			"uid" => $_COOKIE['uid'],
			"hash" => $hash));

		switch( $timetable['status'] ) {
			case "access denied" : $this->view->showMessage("В доступе отказано", "error"); break;
			case "no timetable" : $this->view->showMessage("Расписание еще не заполнено! Сообщите своему старосте!", "error"); break;
			case "ok" : 
				$this->view->showTimetable( $timetable['timetable'] );
				break;
			default: $this->view->showMessage("Ошибка", "error");
		}
	}

}

?>
