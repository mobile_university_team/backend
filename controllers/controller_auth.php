<?php

class controller_auth extends controller {
	
	public function init(){
		$this->view->generateAuthPage();
	}

	public function checkAuth(){
		if( isLogged() ) {
			$this->view->generateFullPage( "main_page" );
			return;
		}

		$arrData = array("email" => $_POST['email'], "pass" => $_POST['pass']);
		$data = json_encode($arrData);
		$key = apiRequest('getPublicKey');
		$key = $key['key'];

		$cryptedData = "";
		
		openssl_public_encrypt($data, $cryptedData, $key);
		$cryptedData = base64_encode($cryptedData);

		$query = array("data" => $cryptedData);

		$ans = apiRequest("auth", "POST", $query);

		if( $ans['status'] === "ok" ) {
			setcookie("token", $ans['token'], 3600*3600*3600, "/");
			setcookie("uid", $ans['uid'], 3600*3600*3600, "/");
			$this->view->generatePageWithMessage( "main_page", "ok", "Вы успешно авторизовались");
		}
		if( $ans['status'] === "failed" ) {
			$this->view->generatePageWithMessage( "auth_page", "error", $ans['message'] );
		}
	}

	public function logOut(){
		setcookie("token", "", time()- 3600, "/");
		setcookie("uid", "", time()- 3600, "/");
		header("Location: /");
	}

	public function register() {
		$ans = apiRequest("registerNewUser", "POST", $_POST);
		if( $ans['status'] == "no data") {
			$this->view->generateMessage( "Не заполнены поля", "error" );
		}
		if( $ans['status'] == "failed" ) {
			$this->view->generateAuthPageWithMessage( "error", $ans['message'] );
		}
		if( $ans['status'] == "ok" ) {
			$this->view->generateAuthPageWithMessage( "ok", "Вы успешно зарегистрировались" );
		}
	}
}

?>
