<!DOCTYPE html>
<html>
<head>
	<meta charset="utf8">
	<link rel="stylesheet" type="text/css" href="/styles/mainStyle.css">
<?php
if( isset($scriptsAndStyles['styleList']) ) {
	foreach($scriptsAndStyles['styleList'] as $name) {
		addStyle($name);
	}
}

if( isset($scriptsAndStyles['scriptList']) ) {
	foreach($scriptsAndStyles['scriptList'] as $name) {
		addScript($name);
	}
}
?>
</head>
<body>
