<form method="POST" action="/auth/register">
Регистрация
<table>

	<tr>
		<td>Email</td>
		<td><input type="text" name="email"></td>
	</tr>

	<tr>
		<td>Имя</td>
		<td><input type="text" name="firstName"></td>
	</tr>
	
	<tr>
		<td>Фамилия</td>
		<td><input type="text" name="secondName"></td>
	</tr>

	<tr>
		<td>Дата рождения</td>
		<td><input type="date" name="birthday"></td>
	</tr>

	<tr>
		<td>Пол</td>
		<td>
			Мужской <input type="radio" name="gender" value="male"><br>
			Женский <input type="radio" name="gender" value="female">
		</td>
	</tr>

	<tr>
		<td>Факультет</td><td>Кафедра</td><td>Курс</td><td>Группа</td><td>Постфикс</td>
	</tr>
	<tr>
		<td>
			<select name="faculty">
				<option value="ЮР">ЮР</option>
				<option value="Э">Э</option>
				<option value="ФН">ФН</option>
				<option value="СМ">СМ</option>
				<option value="ИБМ">ИБМ</option>
				<option value="РК">РК</option>
				<option value="РЛ">РЛ</option>
				<option value="ИУ">ИУ</option>
				<option value="УЦ">УЦ</option>
				<option value="МТ">МТ</option>
				<option value="РТ">РТ</option>
				<option value="РКТ">РКТ</option>
				<option value="ПС">ПС</option>
				<option value="ОЭ">ОЭ</option>
				<option value="АК">АК</option>
				<option value="БМТ">БМТ</option>
			</select>
		</td>
		<td>
			<select name="department">
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
				<option value="8">8</option>
				<option value="9">9</option>
				<option value="10">10</option>
			</select>
		</td>
		<td>
			<select name="course">
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
			</select>
		</td>
		<td>
			<select name="group">
				<option name="1">1</option>
				<option name="2">2</option>
				<option name="3">3</option>
				<option name="4">4</option>
			</select>
		</td>
		<td>
			<select name="postfix">
				<option name=""></option>
				<option name="(Б)">(Б)</option>
				<option name="(М)">(М)</option>
				<option name="Ц (М)">Ц (М)</option>
				<option name="И (М)">И (М)</option>
				<option name="Ц (Б)">Ц (Б)</option>
				<option name="И (Б)">И (Б)</option>
			</select>
		</td>
	</tr>

	<tr>
		<td>Являюсь  старостой</td>
		<td> <input type="checkbox" name="headman"> </td>
	</tr>
	<tr>
		<td>Логин электронного университета</td>
		<td><input type="text" name="loginBmstu"></td>
	</tr>

	<tr>
		<td>Пароль</td>
		<td><input type="password" name="pass"></td>
	</tr>

</table>
<input type="submit" value="Зарегистрироваться">
</form>