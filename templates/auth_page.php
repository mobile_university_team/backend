<?php
if( isLogged() ) {
?>
	Вы уже авторизованы!
	<form method="POST" action="/auth/logOut">
		<input type="submit" value="Выйти">
	</form>
<?php
	exit();
}
?>

<?php

addScript("jquery");
addScript("base64");
addScript("jsencrypt");

$this->generate("auth_form");
$this->generate("registerForm");
?>