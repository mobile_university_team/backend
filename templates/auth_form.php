<?php
addStyle("authForm-style");
?>

<div class="content">
  <div class="form-wrapper">
    <div class="linker"> 
      <span class="ring"></span>
      <span class="ring"></span>
      <span class="ring"></span>
      <span class="ring"></span>
      <span class="ring"></span>
    </div>
    <form class="login-form" action="/auth/checkAuth" method="POST">
      <input type="text" name="email" placeholder="e-mail" />
      <input type="password" name="pass" placeholder="Пароль" />
      <button type="submit">ВОЙТИ</button>
    </form>
  </div>
</div>

<script>
function createDataForRsa(data, key) {
  
  var output = new JSEncrypt(); // создаем объект для шифрования
  output.setPublicKey( key ); // устанавливаем публичный ключ
  output = output.encrypt( data ); // шифруем с помощью публичного ключа
  output =  $.base64.btoa( input ); // кодируем в base64
  return output;
}
</script>