var ACCENT_COLORS = ["#004d40", "#006064", "#00695c", "#00796b", "#00838f", "#00897b", "#0091ea", "#009688", "#0097a7", "#00acc1", "#00bcd4", "#01579b", "#0277bd", "#0288d1", "#039be5", "#03a9f4", "#056f00", "#0a7e07", "#0a8f08", "#0d5302", "#1a237e", "#259b24", "#283593", "#2a36b1", "#303f9f", "#304ffe", "#311b92", "#33691e", "#37474f", "#3949ab", "#3b50ce", "#3d5afe", "#3f51b5", "#4527a0", "#455a64", "#455ede", "#4a148c", "#4d69ff", "#4d73ff", "#4e342e", "#4e6cef", "#512da8", "#536dfe", "#546e7a", "#558b2f", "#5677fc", "#5c6bc0", "#5d4037", "#5e35b1", "#607d8b", "#6200ea", "#651fff", "#673ab7", "#6889ff", "#6a1b9a", "#6d4c41", "#78909c", "#795548", "#7986cb", "#7b1fa2", "#7c4dff", "#7e57c2", "#880e4f", "#8d6e63", "#8e24aa", "#9575cd", "#9c27b0", "#aa00ff", "#ab47bc", "#ad1457", "#b0120a", "#ba68c8", "#bf360c", "#c2185b", "#c41411", "#c51162", "#d01716", "#d500f9", "#d81b60", "#d84315", "#dd191d", "#dd2c00", "#e00032", "#e040fb", "#e51c23", "#e64a19", "#e65100", "#e91e63", "#ef6c00", "#f4511e", "#f50057", "#ff2d6f", "#ff3d00", "#ff4081", "#ff5177", "#ff5722", "#ff6f00", "#ff8f00", "#ffa000"];
var SUBJECT_TYPES = ["Семинар", "Лабораторная работа", "Лекция", "Практическое занятие", "Консультация", "Зачет", "Экзамен"];

var SUBJECT_DEFAULT_TIMES = [{
    "subject_number": 1,
    "time_start": "08:30",
    "time_end": "10:05"
}, {
    "subject_number": 2,
    "time_start": "10:15",
    "time_end": "11:50"
}, {
    "subject_number": 3,
    "time_start": "12:00",
    "time_end": "13:35"
}, {
    "subject_number": 4,
    "time_start": "13:50",
    "time_end": "15:25"
}, {
    "subject_number": 5,
    "time_start": "15:40",
    "time_end": "17:15"
}, {
    "subject_number": 6,
    "time_start": "17:25",
    "time_end": "19:00"
}, {
    "subject_number": 7,
    "time_start": "19:10",
    "time_end": "20:45"
}]

/**
 * @return random number between 0 (inclusive) and 1.0 (exclusive)
 */
function random(seed) {
    seedNumber = 1;

    for (i = 0; i < seed.length; ++i) {
        seedNumber += seed.charCodeAt(i);
    }
    result = Math.sin(seedNumber) * 10000;
    return result - Math.floor(result);
}

function zeroPad(num, numZeros) {
    var n = Math.abs(num);
    var zeros = Math.max(0, numZeros - Math.floor(n).toString().length);
    var zeroString = Math.pow(10, zeros).toString().substr(1);
    if (num < 0) {
        zeroString = '-' + zeroString;
    }
    return zeroString + n;
}

function getAccentColorByString(seed) {
    randomIndex = parseInt((random(seed) * ACCENT_COLORS.length));
    return ACCENT_COLORS[randomIndex];
}

function getMinutesByTime(time) {
    var hours = parseInt(time.split(":")[0]);
    var minutes = parseInt(time.split(":")[1]);
    return hours * 60 + minutes;
}

function getTimeByMinutes(minutes) {
    var hours = parseInt(minutes / 60);
    var minutes = minutes % 60;

    if (minutes === NaN) {
        minutes = 0;
    }

    return zeroPad(hours, 2) + ":" + zeroPad(minutes, 2);
}

function computeBreak(timeStart, timeEnd) {
    var diff = getMinutesByTime(timeEnd) - getMinutesByTime(timeStart);
    var timeToBreak = parseInt((diff - 5) / 2);
    var breakStartMinutes = getMinutesByTime(timeStart) + timeToBreak;
    var breakEndMinutes = breakStartMinutes + 5;
    return {
        break_start: getTimeByMinutes(breakStartMinutes),
        break_end: getTimeByMinutes(breakEndMinutes)
    };
}

var guid = (function() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
  }
  return function() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
           s4() + '-' + s4() + s4() + s4();
  };
})();
