<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang=""en" xml:lang="en""> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang=""en" xml:lang="en""> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang=""en" xml:lang="en""> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 oldie" lang=""en" xml:lang="en""> <![endif]-->
<!--[if gt IE 8]> <html class="no-js newie" lang='"en" xml:lang="en"'> <![endif]-->
<html>
<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="Мобильное приложение для студентов МГТУ им. Н. Э. Баумана">
  <meta name="viewport" content="width=device-width">

  <?php if( isMac() ) { ?>
    <link rel="stylesheet" href="templates/main/css/style.css">
  <?php } else { ?>
    <link rel="stylesheet" href="templates/main/css/style.css">
  <?php } ?>
  


  <link href='http://fonts.googleapis.com/css?family=Roboto:400,300italic,300,100|Roboto+Condensed&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
  <script src="templates/main/js/modernizr-2.6.2.min.js"></script>

  <link rel="icon" type="image/png" href="/favicon.ico" />
  

  <!--Internet Explorer 8 or older doesn't support media query. This script helps ie7 and ie8 to recognize media queries-->
  <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
  <![endif]-->

    
</head>
<body>
  <div id="preloader"></div>
   


  <!-- BEGIN HEADER -->
  <header>
    <div class="container">

      <!-- logo -->
      <a href="" class="logo"></a>
      <!-- end of logo -->

      <!-- Begin of navigation --> 
      <nav>
        <span></span><!-- for showing dropdown menu on click on this span element - uses only for resolution below 768px-->
        <ul>
          <li><a href="#subheader">главная</a></li>
          <li><a href="#features">возможности</a></li>
          <li><a href="#screenshots">скриншоты</a></li>
          <li><a href="#team">команда</a></li>
          <li><a href="#review">обзор</a></li>
          <li><a href="#support">старостам</a></li>
          <li><a href="#pricing">цены</a></li>
          <li><a href="#wait">установить</a></li>
        </ul>
      </nav>
      <!-- End of navigation  -->

    </div>
  </header> 
  <!-- END HEADER -->

  <!-- Begin of #phone  --> 
  <div id="phone" class='phone' 
  data-0="top:102px;"
  data-519="top: 102px" 
  data-520="top: 735px;"
  >
    <div class="clean_subhead">
      <div class="img_wrap">
      </div>
    </div>
    <div class="display">
      <span>CLICK <br>button to start</span>
    </div>
    <div class="phone_head">
      <img src="templates/main/images/phone_header.png" alt="">
    </div>
    <div class="phone_body">
      <a href="" class="start">
        <span></span>
        <b></b>
      </a>
    </div>
  </div>
  <!-- End of #phone  -->

  
  <!-- Begin of .phone_clone --> 
  <!-- visible only on resolution smaller than 768px -->
  <div class="phone_clone">
    <!-- content of this element is generated with JS -->
  </div>
  <!-- End of .phone_clone  -->
  

  <!-- Begin of  .subheader --> 
  <div id="subheader">
    
    <!-- Begin of  .sub_inner --> 
    <div class="sub_inner">
      <div class="container">
        <div class="button_wrapp">
          <a href="" class="apple download">
            <div class='img_wrap'>
              <img src="templates/main/images/apple.png" alt="" class="a">
              <img src="templates/main/images/apple_hover.png" alt="" class="b">
            </div>
            <div class="table">
              <div class="cell">
                <span>Скоро на </span>
                <strong>App Store</strong>
              </div>
            </div>
          </a>
          <a href="" class="google download">
            <div class='img_wrap'>
              <img src="templates/main/images/google.png" alt="" class="a">
              <img src="templates/main/images/google_hover.png" alt="" class="b">
            </div>
            <div class="table">
              <div class="cell">
                <span>Скоро на </span>
                <strong>Google play</strong>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
    <!-- End of .sub_inner  -->
    
    <a href="#features" class="scroll_down">
      прокрутите вниз
    </a>

  </div>
  <!-- End of .subheader  -->

  <!-- BEGIN CONTENT -->
  <div class="content">

    
    <!-- Begin of  .features --> 
    <section id="features">
      <div class="mask m1"></div>
      <div class="mask m2"></div>
      <div class="btn_center"></div>
      <div class="container">
        <h2>Функции</h2>
        <span>Кликните на иконку или соответствующую ссылку для подробной информации</span>
        

        <!-- Begin of  .feature_wrap --> 
        <!--content of .feature .text_wrap is at the bottom of this document. Find element <div class='feature_content'>...</div>
        That content is generated with JS -->
        <div class="feature_wrap">
          <div class="feature f1">
            <a href='#feature1' rel="prettyPhoto" class="img_wrap" >
              <img src="templates/main/images/feature1.png" alt="" class="a">
              <img src="templates/main/images/feature1_hover.png" alt="" class="b">
            </a>
            <!-- if your content of .text is short and don't need link <a class='read_more'>, do not remove whole link, just remove text form link, because of properly work of JS-->
            <div class="text_wrap">
              <div class="text">
                <a href="#feature1" rel="prettyPhoto" class="read_more">Читать далее</a>
              </div>
            </div>
          </div>
          <div class="feature f2">
            <a href='#feature2' rel="prettyPhoto" class="img_wrap">
              <img src="templates/main/images/feature2.png" alt="" class="a">
              <img src="templates/main/images/feature2_hover.png" alt="" class="b">
            </a>
            <div class="text_wrap">
              <div class="text">
                <a href="#feature2" rel="prettyPhoto" class="read_more">Читать далее</a>
              </div>
            </div>
          </div>
          <div class="feature f3">
            <a href='#feature3' rel="prettyPhoto" class="img_wrap">
              <img src="templates/main/images/feature3.png" alt="" class="a">
              <img src="templates/main/images/feature3_hover.png" alt="" class="b">
            </a>
            <div class="text_wrap">
              <div class="text">
                <a href="#feature3" rel="prettyPhoto" class="read_more">Читать далее</a>
              </div>
            </div>
          </div>
          <div class="feature f4">
            <a href='#feature4' rel="prettyPhoto" class="img_wrap">
              <img src="templates/main/images/feature4.png" alt="" class="a">
              <img src="templates/main/images/feature4_hover.png" alt="" class="b">
            </a>
            <div class="text_wrap">
              <div class="text">
                <a href="#feature4" rel="prettyPhoto" class="read_more">Читать далее</a>
              </div>
            </div>
          </div>
        </div>
        <!-- End of .feature_wrap  -->

      </div>
    </section>
    <!-- End of .features  -->


          
    <!-- Begin of #screenshots  --> 
    <section id="screenshots">
      <div class="container">
        <h2>Скриншоты</h2>
        <span>Перелистывайте скриншоты или кликайте на стрелки для навигации</span>

        <!-- Begin of  .phone_slider --> 
        <div id="phone_slider" class="owl-carousel">
          <div class="item">
            <!-- Begin of  .p1 --> 
            <div class="phone p1">
              <div class="screenshot">
                <img src="templates/main/images/screenshot2.png" alt="">
              </div>
              <div class="phone_head">
                <img src="templates/main/images/phone_header.png" alt="">
              </div>  
              <div class="phone_body">
              </div>
            </div>
            <!-- End of .p1  -->          
          </div>
          <div class="item">
            <!-- Begin of  .p2 --> 
            <div class="phone p2">
              <div class="screenshot">
                <img src="templates/main/images/screenshot3.png" alt="">
              </div>
              <div class="phone_head">
                <img src="templates/main/images/phone_header.png" alt="">
              </div>  
              <div class="phone_body">
              </div>
            </div>
            <!-- End of .p2  -->          
          </div>
          <div class="item">
            <!-- Begin of  .p3 --> 
            <div class="phone p3">
              <div class="screenshot">
                <img src="templates/main/images/screenshot1.png" alt="">
              </div>
              <div class="phone_head">
                <img src="templates/main/images/phone_header.png" alt="">
              </div>  
              <div class="phone_body">
              </div>
            </div>
            <!-- End of .p3  -->          
          </div>
          <div class="item">
            <!-- Begin of  .p1 --> 
            <div class="phone p1">
              <div class="screenshot">
                <img src="templates/main/images/screenshot2.png" alt="">
              </div>
              <div class="phone_head">
                <img src="templates/main/images/phone_header.png" alt="">
              </div>  
              <div class="phone_body">
              </div>
            </div>
            <!-- End of .p1  -->          
          </div>
          <div class="item">
            <!-- Begin of  .p2 --> 
            <div class="phone p2">
              <div class="screenshot">
                <img src="templates/main/images/screenshot3.png" alt="">
              </div>
              <div class="phone_head">
                <img src="templates/main/images/phone_header.png" alt="">
              </div>  
              <div class="phone_body">
              </div>
            </div>
            <!-- End of .p2  -->          
          </div>
          <div class="item">
            <!-- Begin of  .p3 --> 
            <div class="phone p3">
              <div class="screenshot">
                <img src="templates/main/images/screenshot1.png" alt="">
              </div>
              <div class="phone_head">
                <img src="templates/main/images/phone_header.png" alt="">
              </div>  
              <div class="phone_body">
              </div>
            </div>
            <!-- End of .p3  -->          
          </div>
          <div class="item">
            <!-- Begin of  .p2 --> 
            <div class="phone p2">
              <div class="screenshot">
                <img src="templates/main/images/screenshot4.png" alt="">
              </div>
              <div class="phone_head">
                <img src="templates/main/images/phone_header.png" alt="">
              </div>  
              <div class="phone_body">
              </div>
            </div>
            <!-- End of .p2  -->          
          </div>
        </div>
        <!-- End of .phone_slider  -->
        <div class="slider_navigation">
          <a class="prev">Назад</a>
          <a class="next">Вперед</a>
        </div>       
      </div>
    </section>
    <!-- End of #screenshots  -->
          
    
    <!-- Begin of #review  --> 
    <section id="review">
      <div class="container">
        <h2>Обзор приложения</h2>
        <span>Посмотрите приложение в действии</span>
        <div id="video_section">
          <!-- instructions for video embed:
          http://www.jonsuh.com/demo/responsive-youtube-vimeo-embed-html5-videos-with-css -->
          <!-- https://github.com/jonsuh/Responsive-YouTube-Vimeo-Embed-and-HTML5-Videos-with-CSS/blob/master/demo.html -->
          <div class="video_wrap">
            <!-- vimeo example -->
            <div class="js-video widescreen vimeo">
              <div class="lazy_load_video vimeo" data-vimeo-id="78957784"></div>
            </div> 

            <!-- youtube example -->
            <!-- <div class="js-video widescreen">
              <div class="lazy_load_video youtube" data-youtube-id="-1nnJtWSxn0"></div>              
            </div>   -->

          </div>
        </div>        
      </div>
    </section>
    <!-- End of #review  -->


    <!-- Begin of #support  --> 
    <section id="support">
      <div class="container">
        <div class="phone_x2">
          <div class="p1">
            <img src="templates/main/images/support_bgr1.png" alt="" class='bgr_img'>
            <img src="templates/main/images/support_img1.png" alt="" class='main_img'>
          </div>
          <div class="p2 hidden">
            <img src="templates/main/images/support_bgr2.png" alt="" class='bgr_img'>
            <img src="templates/main/images/support_img2.png" alt="" class='main_img'>
          </div>
        </div>
        <div class="support_form">
          <h2>Старостам</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pellentesque lectus id felis vehicula interdum. Aliquam erat volutpat. Aliquam augue diam, pulvinar quis iaculis id.</p>
          <form action="">
            <input type="text" placeholder='Name'>
            <input type="email" placeholder='E-mail'>
            <input type="tel" placeholder='Phone'>
            <textarea placeholder='Message'></textarea>
            <button type='submit' class="btn right">отправить</button>
          </form>
        </div>
      </div>
    </section>
    <!-- End of #support  -->

    <!-- Begin of #wait  --> 
    <section id="wait">
      <h2>Пора начинать!</h2>
      <span>Оформите бесплатную пробную подписку на 30 дней прямо сейчас!</span>
      <button class='btn'>Установить приложение</button>
    </section>
    <!-- End of #wait  -->
    
    <!-- Begin of #work  --> 
<!--     <section id="work">
      <div class="container">
        <h2>Other works</h2>
        <span>Donec lorem justo, adipiscing vitae cursus non, vehicula in metus.</span>

        <ul>
          <li>
            <div><a href=""><img src="templates/main/images/other/img1.png" alt=""></a></div>
          </li>
          <li>
            <div><a href=""><img src="templates/main/images/other/img2.png" alt=""></a></div>
          </li>
          <li>
            <div><a href=""><img src="templates/main/images/other/img3.png" alt=""></a></div>
          </li>
          <li>
            <div><a href=""><img src="templates/main/images/other/img4.png" alt=""></a></div>
          </li>
          <li>
            <div><a href=""><img src="templates/main/images/other/img5.png" alt=""></a></div>
          </li>
          <li>
            <div><a href=""><img src="templates/main/images/other/img6.png" alt=""></a></div>
          </li>
        </ul>
      </div>
    </section> -->
    <!-- End of #work  -->
    
    <!-- Begin of #get  --> 
<!--     <section id="get">
      <div class="conteiner">
        <h2>Want this awesome one page theme? Get it right now!</h2>
        <a href=''>
          <i></i>
          <b></b>
        </a>
      </div>
    </section> -->
    <!-- End of #get  -->
    
    <!-- Begin of #notified  --> 
    <section id="notified">
      <div class="container">
        <form action="">
          <h2>Узнайте первым о запуске проекта</h2>
          <input type="email" placeholder='Введите свой e-mail адрес'>
          <button class="btn">Подписаться</button>
        </form>
      </div>
    </section>
    <!-- End of #notified  -->
                    
  <a href="#subheader" id="to_the_top"></a>

  </div>
  <!-- END CONTENT -->

  <!-- BEGIN FOOTER -->
  <footer>
    <div class="container">
      <small>Mobile University &copy; 2014. All rights reserved.</small>
      <ul class='social'>
        <li><a href="" class='social-fb'></a></li>
        <li><a href="" class='social-tw'></a></li>
        <li><a href="" class='social-bb'></a></li>
        <li><a href="" class='social-apple'></a></li>
        <li><a href="" class='social-an'></a></li>
      </ul>
    </div>
  </footer> 
  <!-- END FOOTER -->

  
  <!-- Begin of  .feature_content --> 
  <div class="feature_content">
    
    <!-- Begin of #feature1  --> 
    <div id="feature1" class="f1">
      <div class="text_wrapper">
        <h3>Расписание</h3>
        <div class="text">
          <p>Актуальное расписание всегда под рукой</p>
          <p>Теперь расписание, всегда у вас в кармане, следите за всеми изменениями и отменами пар</p>
        </div>
      </div>
    </div>
    <!-- End of #feature1  -->
    
    <!-- Begin of #feature2  --> 
    <div id="feature2" class="f2">
      <div class="text_wrapper">
        <h3>Справочник</h3>
        <div class="text">
          <p>Справочник учреждений в МГТУ им. Баумана.</p>
          <p>Наконец можно без труда найти контакты музея мгту им баумана, и посмотреть часы приема на кафедре ИУ5. И все это у вас под рукой.</p>
        </div>
      </div>
    </div>
    <!-- End of #feature2  -->
    
    <!-- Begin of #feature3  --> 
    <div id="feature3" class="f3">
      <div class="text_wrapper">
        <h3>Преподаватели</h3>
        <div class="text">
          <p>Полный список преподавателей МГТУ им. Баумана
          <p>   
Больше не потребуется тратить большое количество времени на расспросы студентов о преподах, просто зайдите в Мобильный университет, и сами посмотрите где нужный преподаватель в данный момент.</p>
        </div>
      </div>
    </div>
    <!-- End of #feature3  -->
    
    <!-- Begin of #feature4  --> 
    <div id="feature4" class="f4">
      <div class="text_wrapper">
        <h3>Столовые</h3>
        <div class="text">
          <p>Nec essit atibus rep reh end erit moles tias suscipit odit pariatur dolorum saepe ut?
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, debitis, facere dolorum dolore natus optio veritatis dolorem quod perferendis temporibus nisi explicabo delectus consectetur distinctio aut vero similique. Nam, voluptatem!</p>
          <p>Nec essit atibus rep reh end erit moles tias suscipit odit pariatur dolorum saepe ut?
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi, debitis, facere dolorum dolore natus optio veritatis dolorem quod perferendis temporibus nisi explicabo delectus consectetur distinctio aut vero similique. Nam, voluptatem!</p>
        </div>
      </div>
    </div>
    <!-- End of #feature4  -->
    
  </div>
  <!-- End of .feature_content  -->
  

  <!-- JavaScript at the bottom for fast page loading -->
  <script src="templates/main/js/jquery-1.10.2.min.js"></script>
  <script src="templates/main/js/jquery.autosize.js"></script>
  <script src="templates/main/js/skrollr.min.js"></script>
  <script src="templates/main/js/selectivizr-min.js"></script>
  <script src="templates/main/js/owl.carousel.min.js"></script>
  <script src="templates/main/js/jquery.inview.min.js"></script>
  <script src="templates/main/js/jquery.prettyPhoto.js"></script>
  <script src="templates/main/js/jquery.smooth-scroll.min.js"></script>  
  <script src="templates/main/js/init.js"></script>

  <!-- Make visible media queries to IE7 , IE8 (visible only on server or localhost) -->
    <link href="http://externalcdn.com/respond-proxy.html" id="respond-proxy" rel="respond-proxy" />
    <script src="templates/main/js/respond.src.js"></script>

    
</body>
</html>
