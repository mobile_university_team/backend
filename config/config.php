<?php

define("LOG_PATH", __DIR__."/../logi");
define("API_VERSION", "v1");

ini_set("session.use_only_cookies", 1);

function exceptionHandler($exception) {
	putLog( LOG_PATH, $exception->getMessage()." ".$exception->getLine()." ".$exception->getFile() );
	echo "Ошибка";
	die();
}

function errorHandler($exception) {

}

set_exception_handler("exceptionHandler");


$PRIVILEGIES = array("admin", "moderator", "reader", "guest");
?>
