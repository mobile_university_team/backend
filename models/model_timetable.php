<?php

class model_timetable extends model {
	public function createTimetable($group, $timetable) {
		if( mDB::getInstance()->isInCollection("groupsTimetable", array("group" => $group)) ) {
			mDB::getInstance()->remove("groupsTimetable", array("group" => $group));
		}
		mDB::getInstance()->insert( "groupsTimetable", array("timetable" => $timetable, "group" => $group) );
		return true;
	}

	public function getTimetable($group) {
		$timetable = mDB::getInstance()->find("groupsTimetable", array( "group" => $group ));
		return $timetable;
	}
}

?>