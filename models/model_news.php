<?php

class News {

	public $source_address;
	public $date;
	public $title;
	public $content;
	
	public function __construct($s, $d, $t, $c) {
		$this->source_address = $s;
		$this->date = $d;
		$this->title = $t;
		$this->content = $c;
	}
}

class model_news extends model {
	
	private $sources = array (
		"vk" => array(
			"technopark" => 'owner_id=-45881160',
			"priznavashki" => 'owner_id=-59336460',
			),
		"technopark" => '');

	public function getNewsFromVk($groupId = '-59336460', $numOfNews = 5, $source = "vk.com/lovebmstu") {

		$ch = curl_init("https://api.vk.com/method/wall.get?owner_id=".$groupId."&domain=".$source."&count=".$numOfNews);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$answer = curl_exec($ch);
		curl_close($ch);
		$answer = json_decode($answer)->response;

		$news = array();

		for($i = 1; $i<count($answer); $i++){
			$source_address = $source;
			$title = cutText( $answer[$i]->text, 30);

			$date = getdate($answer[$i]->date);
			$date = $date['year']."-".$date['mon']."-".$date['mday']." ".$date['hours'].":".$date['minutes'].":".$date['seconds'];
			
			$content = $answer[$i]->text;

			$news[] = new News($source_address, $date, $title, $content);
		}

		return $news;
		
	}

	public function getNewsFromBmstu() {

	}
	public function getNewsFromTechnopark() {

	}

}
?>