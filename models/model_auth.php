<?php

class model_auth extends model{

	public function checkAuth($logs) {
		$email = $logs['email'];
		$pass = md5( $logs['pass'] );

		$query = array("email" => $email, "pass" => $pass);

		try {
			$user = mDB::getInstance()->find("users", $query);
		} catch( Exception $e) {
			putLog( LOG_PATH, $e->getMessage() );
			die();
		}

		if( !empty($user) ) {
			User::setUser($user[0]);
			return true;
		}
		throw new Error("Такого пользователя не существует");
	}

	public function registerNewUser($data) {
		$d = $data;

		if( isset($data['headman']) ) {
			$headmanIsAlreadyExists = mDB::getInstance()->isInCollection( "users", array("group" => $data['group'], "headman" => true) );
			if( $headmanIsAlreadyExists ) {
				throw new Error("В вашей группе уже есть староста! Обратитесь к нему, чтобы он передал вам полномочия");
			}
			$data['headman'] = true;
		}
		
		if( empty($data['postfix']) ) {
			unset( $data['postfix'] );
		}

		foreach( $data as $key => $val) {
			if( empty($val) ) {
				throw new Error("Одно из полей пропущено");
			}
		}

		if( mDB::getInstance()->isInCollection("users", array("email" => $data['email'])) ) {
			throw new Error("Такой пользователь уже существует");
		}
		
		$data['pass'] = md5($data['pass']);
		$data['rights'] = "reader";

		try {
			$res = mDB::getInstance()->insert("users", $data);
		} catch( Exception $e ) {
			putLog( LOG_PATH, $e->getMessage() );
			throw $e;
		}

		return $this->checkAuth($d);
	}
}

?>