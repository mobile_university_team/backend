<?php

class model_admin extends model{
	
	public function addUser($data) {

		$data['pass'] = md5($data['pass']);

		if( mDB::getInstance()->isInCollection( "users", array('email' => $data['email'] ) ) ) {
			throw new Error("Такой пользователь уже существует");
		}

		if( $data['headman'] ) {
			if( mDB::getInstance()->isInCollection("users", array("group" => $data['group'], "headman" => $data['headman'])) ) {
				throw new Error("В группе уже есть староста");
			}
		} else {
			unset($data['headman']);
		}
		$PRIVILEGIES = array("admin", "moderator", "reader");
		if( !in_array($data['rights'], $PRIVILEGIES) ) {
			throw new Error("Недопустимый уровень прав!");
		}
		
		mDB::getInstance()->insert("users", $data);
	}

	public function addOrganization($data) {
		if ( hasEmpty($data['orgInfo']) ) {
			throw new Error("Одно из полей пропущено");
		}
		
		$data['orgInfo'] = (array) $data['orgInfo'];

		$isAlreadyExists = mDB::getInstance()->isInCollection( "organizationList", array("name" => $data['orgInfo']['name']));
		
		if( $isAlreadyExists === true ) {
			throw new Error("Такая организация уже существует");
		}

		$organizationId = mDB::getInstance()->insert( "organizationList", $data['orgInfo'] );

		if( isset( $data['timetable'] ) &&
			!isEmpty( $data['timetable'] )) {
			$data['timetable'] = (array) $data['timetable'];
			
			foreach( $data['timetable'] as $day => $time) {
				if( !empty( $time->timeFrom ) ) {
					$query = array(
						"dayOfWeek" => "",
						"timeFrom" => "",
						"timeTo" => ""
					);
					$query['dayOfWeek'] = (string) $day;
					$query['timeFrom'] = (string) $time->timeFrom;
					if( !empty( $time->timeTo ) ) {
						$query['timeTo'] = (string) $time->timeTo;
					}
					$query['id'] = $organizationId;
					mDB::getInstance()->insert( "timetableList", $query );
				}
			}
		}
	}
}

?>