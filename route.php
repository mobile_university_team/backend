<?php

class request {
	public static $request;
	public static function init() {

		self::$request = array(
			"controller" => "",
			"action" => "",
			"query" => "",
			"data" => ""
		);

		/*if( false ) {
			self::$request['controller'] = "main";
		}*/ /*else {*/
			$ar_request = explode("?", $_SERVER['REQUEST_URI']);
			if( isset($ar_request[1]) && !empty($ar_request[1]) ) {
				self::$request['data'] = $ar_request[1];
			}
			$ar_request = explode("/", $ar_request[0]);
			if(isset($ar_request[1]) && !empty($ar_request[1])) {
				self::$request['controller'] = $ar_request[1];
			} else {
				self::$request['controller'] = "main";
			}
			if(isset($ar_request[2]) && !empty($ar_request[2])) {
				self::$request['action'] = $ar_request[2];
			}
			if( count($ar_request) > 3 ) {
				self::$request['query'] = array_slice($ar_request, 3);
			}
		// }
	}

	public static function getController() {
		return self::$request['controller'];
	}

	public static function getAction() {
		return self::$request['action'];
	}

	public static function getQuery() {
		return self::$request['query'];
	}

	public static function getData() {
		return self::$request['data'];
	}
}

class apiError extends Exception {

	protected $status;

	public function __construct($status, $message) {
		$this->status = (int)$status;
		parent::__construct($message);
	}

	public function getStatus() {
		return $this->status;
	}
}

class Route{

	private static $route;
	
	public static function start(){
		request::init();


		if( request::getController() === "api" ) {
			self::$route = new apiRoute();
			self::$route->start();
			return;

		} else {
			
			try {

				$model = $controller = $view = request::getController();
				$action = request::getAction();

				$file_model = "models/model_".$model.".php";
				$file_controller = "controllers/controller_".$controller.".php";
				$file_view = "views/view_".$view.".php";

				if( empty($action) ) {
					$action = "init";
				}

				if(file_exists($file_controller)) {
					include $file_controller;
				}
				else {
					throw(new Exception("Не найден файл ".$file_controller));
				}

				if(file_exists($file_model)) {
					include $file_model;
				}

				if(file_exists($file_view)) {
					include $file_view;
				}

				$ob_controller = "controller_".$controller;
				$ob_controller = new $ob_controller;

				if(method_exists($ob_controller, $action)) {
					try {
						$ob_controller->$action();
					} catch ( Exception $e ) {
						$message = $e->getMessage();
						putLog(LOG_PATH, $message);
					}
				} else {
					throw new Exception("Данное действие контроллера отсутствует: ".$action);
				}

			} catch( Exception $e ) {
				$message = $e->getMessage();
				echo $message;
				header('HTTP/1.1 404 Not Found');
				header("Status: 404 Not Found");
				include __DIR__."/404.php";
			}
		}
	}
				
}

class apiRoute {

	public function start() {

		try {

			$version = request::getAction();
			if( empty($version) ) {
				throw new apiError(400, "Вы не указали версию api");
			}

			if( !file_exists("api/".$version."/index.php") ) {
				throw new apiError(404, "Вы указали не правильную версию api");
			}

			include "api/".$version."/index.php";

			Starter::start();
			return;

		} catch( apiError $e ) {
			
			// echo "<meta charset='utf8'>";
			// var_dump($e->getMessage());
			
			apiAnswer( array("status" => FAIL, "message" => $e->getMessage()) );

		}
	}
}
?>
